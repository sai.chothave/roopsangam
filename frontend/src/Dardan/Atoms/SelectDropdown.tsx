import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { FormControl, FormHelperText, InputLabel, MenuItem, Select } from '@mui/material';

const SelectDropdown = (props: any) => {
    const {
        name, labelName, touched, errorMessage, options, value = "", onChange, placeHolder, required, ...rest
    } = props

    const [selectValue, setSelectValue] = useState(value);
    const handleOnChange = (evt: any) => {
        const value = evt.target.value;
        setSelectValue(value)
        onChange(value)
    }
    return (
        <FormControl fullWidth> 
            {
                labelName ? (<InputLabel >{labelName}</InputLabel>) : null
            }
            <Select id={name} name={name} {...rest} value={selectValue} variant={"standard"} onChange={handleOnChange}>
                {
                    options.map((option: any, index: number) => 
                        <MenuItem value={option.id} key={index}>
                            {option.value}
                        </MenuItem>
                        )
                }
            </Select>
            {!touched && errorMessage && (
                <FormHelperText sx={{color:"red"}}>{errorMessage}</FormHelperText>
            )}
            {touched && errorMessage && (
                <FormHelperText sx={{color:"red"}}>{errorMessage}</FormHelperText>
            )}
        </FormControl>
    )
}

export default SelectDropdown