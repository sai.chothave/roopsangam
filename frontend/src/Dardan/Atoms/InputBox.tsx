import React from 'react'
import PropTypes from 'prop-types'
import { FormControl, FormControlLabel, FormHelperText, InputLabel, TextField } from '@mui/material';

const InputBox = (props: any) => {
    const { name, labelName, touched, errorMessage, required, value = "", ...rest } = props;
    return (
        <FormControl fullWidth>
            {/* {
                labelName ? (<InputLabel >{labelName}</InputLabel>) : null
            } */}
            {/* <input name={name} {...rest} value={value}/> */}
            <TextField name={name} {...rest} label={labelName} value={value} variant={"standard"}/>
            {!touched && errorMessage && (
                <FormHelperText sx={{color:"red"}}>{errorMessage}</FormHelperText>
            )}
            {touched && errorMessage && (
                <FormHelperText sx={{color:"red"}}>{errorMessage}</FormHelperText>
            )}
        </FormControl>
    )
}

// InputBox.propTypes = {
//     name:PropTypes.string,
//     labelName:PropTypes.string,
//     touched:PropTypes.bool,
//     errorMessage:PropTypes.string,
//     value:PropTypes.string,
//     required:PropTypes.bool,
// };

// InputBox.propTypes = {
//     name:"",
//     labelName:"",
//     touched:false,
//     errorMessage:"",
//     value:"",
//     required:false,
// };

export default InputBox