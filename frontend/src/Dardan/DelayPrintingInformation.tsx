import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import SelectDropdown from './Atoms/SelectDropdown'
import InputBox from './Atoms/InputBox'
import { Accordion, AccordionSummary, Grid, Typography } from '@mui/material'
import { ExpandMore } from '@mui/icons-material'

const DelayPrintingInformation = (props: any) => {
  const { value, touched, errors, handleInputChange, handleSelectChange, requireds,options } = props

  useEffect(()=>{
    console.log("DPI",value)
  },[])

  return (
    <Accordion sx={{ margin: "20px", padding: 4 }} expanded>
      <AccordionSummary expandIcon={<ExpandMore />} >
        <Typography variant='h5' sx={{ width: '33%', flexShrink: 0 }}>
          Edit {"sa"}
        </Typography>
      </AccordionSummary>
      <Grid container columns={5} columnGap={3} rowGap={5}>
        <Grid item md={1}>
          <InputBox
            type="text"
            labelName="delayPrintingInfoName"
            placeholder="Label"
            name="delayPrintingInfoName"
            value={value.delayPrintingInfoName}
            touched={touched.delayPrintingInfoName}
            errorMessage={errors.delayPrintingInfoName}
            onChange={handleInputChange}
            required={requireds.delayPrintingInfoName}
            data-testid="EventBox" />
        </Grid>
        <Grid item md={1}>
          <SelectDropdown
            labelName="Order Source"
            name="orderSource"
            placeHolder="Select"
            options={options.orderSourceReferenceData}
            value={value.orderSource.orderSourceId}
            touched={touched.orderSource}
            errorMessage={errors.orderSource}
            onChange={(val: any) => handleSelectChange(val, "orderSource")}
            required={requireds.orderSource} />
        </Grid>
        <Grid item md={1}>
          <SelectDropdown
            labelName="Order Channel"
            name="orderChannel"
            placeHolder="Select"
            options={options.orderChannelReferenceData}
            value={value.orderChannel.orderChannelId}
            touched={touched.orderChannel}
            errorMessage={errors.orderChannel}
            onChange={(val: any) => handleSelectChange(val, "orderChannel")}
            required={requireds.orderChannel} />
        </Grid>
        <Grid item md={1}>
          <SelectDropdown
            labelName="Order Type"
            name="orderType"
            placeHolder="Select"
            options={options.orderTypeReferenceData}
            value={value.orderType.orderTypeId}
            touched={touched.orderType}
            errorMessage={errors.orderType}
            onChange={(val: any) => handleSelectChange(val, "orderType")}
            required={requireds.orderType} />
        </Grid>

        <Grid item md={1}>
          <SelectDropdown
            labelName="Print Option"
            name="printOption"
            placeHolder="Select"
            options={options.printOptionReferenceData}
            value={value.printOption.printOptionId}
            touched={touched.printOption}
            errorMessage={errors.printOption}
            onChange={(val: any) => handleSelectChange(val, "printOption")}
            required={requireds.printOption} />
        </Grid>
        <Grid item md={1}>
          <InputBox
            type="number"
            labelName="Group Ticket Pickup Timeframe"
            placeholder="GTPT"
            name="groupTicketPickupTimeFrame"
            value={value.groupTicketPickupTimeFrame}
            touched={touched.groupTicketPickupTimeFrame}
            errorMessage={errors.groupTicketPickupTimeFrame}
            onChange={handleInputChange}
            required={requireds.groupTicketPickupTimeFrame}
            disabled={(value.printOption.printOptionName==="Group Tickets")?false:true}
            data-testid="EventBox" />
        </Grid>
        <Grid item md={1}>
          <InputBox
            type="number"
            labelName="Group Ticket Offset Time for Printing"
            placeholder="GTPT"
            name="groupTicketOffsetTime"
            value={value.groupTicketOffsetTime}
            touched={touched.groupTicketOffsetTime}
            errorMessage={errors.groupTicketOffsetTime}
            onChange={handleInputChange}
            required={requireds.groupTicketOffsetTime}
            disabled={(value.printOption.printOptionName==="Group Tickets")?false:true}
            data-testid="EventBox" />
        </Grid>
        <Grid item md={1}>
          <InputBox
            type="number"
            labelName="Kitchen Offset Time for Printing"
            placeholder="GTPT"
            name="kitchenOffsetTime"
            value={value.kitchenOffsetTime}
            touched={touched.kitchenOffsetTime}
            errorMessage={errors.kitchenOffsetTime}
            onChange={handleInputChange}
            required={requireds.kitchenOffsetTime}
            disabled={(value.printOption.printOptionName==="Goes to kitchen")?false:true}
            data-testid="EventBox" />
        </Grid>

        <Grid item md={1}>
          <SelectDropdown
            labelName="Destination Queue"
            name="destinationDeviceQueueId"
            placeHolder="Select"
            options={options.destinationQueuesReferenceData}
            value={value.destinationDeviceQueueId.destinationQueueId}
            touched={touched.destinationDeviceQueueId}
            errorMessage={errors.destinationDeviceQueueId}
            onChange={(val: any) => handleSelectChange(val, "destinationDeviceQueueId")}
            required={requireds.destinationDeviceQueueId} />
        </Grid>


      </Grid>
    </Accordion>
  )
}

export default DelayPrintingInformation