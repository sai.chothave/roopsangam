import React, { useCallback, useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import DelayPrintingInformation from './DelayPrintingInformation'
import { useLocation } from 'react-router-dom'
import { formdataLength, formdataLengthError, formdataPattern, formdataPatternError, initialOptions, initialValue, initialValueTouched, requiredKeys } from './constant'
import { Button } from '@mui/material'

const EditDelayPrinting = (props) => {

  const location = useLocation()
  const [touched, setTouched] = useState({ ...initialValueTouched })
  const [formData, setFormData] = useState()
  const [formErrors, setFormErrors] = useState({ ...initialValue })
  const [initialFormData, setInitialFormData] = useState()
  const [disableSavebutton, setDisableSavebutton] = useState(true)
  const [requiredFields, setRequiredFields] = useState({ ...requiredKeys })

  const [options, setOptions] = useState({ ...initialOptions })

  const convertOptions = () => {
    for (const option in options) {
      options[option].map((field) => {
        field["id"] = field[Object.keys(field)[0]]
        field["value"] = field[Object.keys(field)[1]]
      })
    }
  }

  const getSDValueByIDandField = (id,field) => {
    field += "ReferenceData"
    const value = {...options[field].filter((val)=>val.id===id)[0]}
    delete value["id"]
    delete value["value"]
    return value
  }

  const isRequired = (name) => {
    return requiredKeys.indexOf(name) !== -1
  }

  const validateField = useCallback((name, value) => {
    let pattern = formdataPattern[name]
    let length = formdataLength[name]
    let validationObject = { validationCheck: false, error: "" }
    value = String(value)
    if(Number.isInteger(value) || Number.isNu)
    if (value.length < 1 && (value !== 0 || value !== "0") && isRequired(name))
      validationObject = { validationCheck: true, error: "Required" }
    else if (!value.match(pattern))
      validationObject = { validationCheck: true, error: formdataPatternError[name] }
    else if (value.length >= length)
      validationObject = { validationCheck: true, error: formdataLengthError[name] }
    console.log(value.length > length)
    if (!validationObject.validationCheck) {
      setFormErrors({ ...initialValue })
    }

    return validationObject
  }, [])

  const enableSaveButton = (name, value) => {
    if (value !== initialFormData[name])
      setDisableSavebutton(false)
  }

  const handleInputChange = useCallback((event) => {
    const name = event.target.name
    const value = event.target.value
    setFormData({ ...formData, [name]: value })
    let validation = validateField(name, value)
    if (validation.validationCheck) {
      setFormErrors({ ...formErrors, [name]: validation.error })
    } else {
      enableSaveButton(name, value)
    }
    console.log("Value Changed ", name, " => ", value)
  }, [formData, disableSavebutton])

  const handleSelectChange = useCallback((value, name) => {
    const selectedValue = getSDValueByIDandField(value,name)
    setFormData({...formData,[name]:selectedValue})
    let validation = validateField(name, selectedValue)
    if (validation.validationCheck) {
      setFormErrors({ ...formErrors, [name]: validation.error })
    } else {
      enableSaveButton(name, selectedValue)
    }
    console.log("Value Changed ", name, " => ", selectedValue)
  }, [formData, disableSavebutton])

  const readValuesFromLocationState = useCallback(() => {
    setFormData({ ...location.state })
    setInitialFormData({ ...location.state })
  }, [location.state])

  const saveDetails = () => {
    console.log("API REQUEST",formData)
  }

  useEffect(() => {
    readValuesFromLocationState()
    console.log("EDP ", { ...location.state })
    convertOptions()
  }, [])

  return (
    <div>
      {formData && 
        <DelayPrintingInformation value={formData} touched={touched} errors={formErrors} handleInputChange={handleInputChange} handleSelectChange={handleSelectChange} requireds={requiredFields} options={options} />
      }
      <Button variant='contained' disabled={disableSavebutton} onClick={saveDetails}>Save</Button>
    </div>

  )
}

EditDelayPrinting.propTypes = {}

export default EditDelayPrinting