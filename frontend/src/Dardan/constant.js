export const initialValue = {
    delayPrintingInfoId:"",
    delayPrintingInfoName:"",
    orderSource:"",
    orderChannel:"",
    orderType:"",
    printOption:"",
    groupTicketPickupTimeFrame:"",
    groupTicketOffsetTime:"",
    kitchenOffsetTime:"",
    destinationDeviceQueueId:"",
    autoPrintingFlag:false,
}

export const initialValueTouched = {
    delayPrintingInfoId:false,
    delayPrintingInfoName:false,
    orderSource:false,
    orderChannel:false,
    orderType:false,
    printOption:false,
    groupTicketPickupTimeFrame:false,
    groupTicketOffsetTime:false,
    kitchenOffsetTime:false,
    destinationDeviceQueueId:false,
    autoPrintingFlag:false,
}


export const formdataPattern = {
    delayPrintingInfoName:/^[a-zA-Z0-9 -\s@#$!&_.%]+$/,
    groupTicketPickupTimeFrame:/^[0-9]{1,4}$/,
    groupTicketOffsetTime:/^[0-9]{1,4}$/,
    kitchenOffsetTime:/^[0-9]{1,4}$/,
}

export const formdataLength = {
    delayPrintingInfoName:40,
    groupTicketPickupTimeFrame:4,
    groupTicketOffsetTime:4,
    kitchenOffsetTime:4,
}


export const formdataPatternError = {
    delayPrintingInfoName:"The Following special chars",
    orderSource:"",
    orderChannel:"",
    orderType:"",
    printOption:"",
    groupTicketPickupTimeFrame:"Maximum value is 1440",
    groupTicketOffsetTime:"Maximum value is 1440",
    kitchenOffsetTime:"Maximum value is 1440",
    destinationDeviceQueueId:"",
}

export const formdataLengthError = {
    delayPrintingInfoName:"Must be 40 or less chars",
    orderSource:"",
    orderChannel:"",
    orderType:"",
    printOption:"",
    groupTicketPickupTimeFrame:"Must be 4 or less chars",
    groupTicketOffsetTime:"Must be 4 or less chars",
    kitchenOffsetTime:"Must be 4 or less chars",
    destinationDeviceQueueId:"",
}

export const requiredKeys = [
    "delayPrintingInfoName",
    "orderSource",
    "orderChannel",
    "orderType",
    "printOption",
    "destinationDeviceQueueId",
]


export const initialOptions = {
    orderTypeReferenceData: [
        {
            orderTypeId:1,
            orderTypeName:"CateringDelivery",
        },{
            orderTypeId:2,
            orderTypeName:"CateringPickup",
        },{
            orderTypeId:3,
            orderTypeName:"InResturant",
        },{
            orderTypeId:4,
            orderTypeName:"SmallOrderDelivery",
        },{
            orderTypeId:5,
            orderTypeName:"SmallOrderPizckup",
        },
    ],
    orderSourceReferenceData : [
        {
            orderSourceId: 1,
            orderSourceName:"DP"
        },{
            orderSourceId: 2,
            orderSourceName:"ezCater"
        },{
            orderSourceId: 3,
            orderSourceName:"Order Ahead"
        },{
            orderSourceId: 4,
            orderSourceName:"POS"
        },{
            orderSourceId: 5,
            orderSourceName:"Ziosk"
        },
    ],
    printOptionReferenceData:[
        {
            printOptionId:1,
            printOptionName:"Check Created"
        },{
            printOptionId:2,
            printOptionName:"Do not auto print"
        },{
            printOptionId:3,
            printOptionName:"Goes to kitchen"
        },{
            printOptionId:4,
            printOptionName:"Group Tickets",
        },
    ],
    orderChannelReferenceData:[
        {
            orderChannelId:1,
            orderChannelName:"InResturant"
        },{
            orderChannelId:2,
            orderChannelName:"OnLine"
        },{
            orderChannelId:3,
            orderChannelName:"Phone"
        },{
            orderChannelId:4,
            orderChannelName:"ThirdParty"
        }
    ],
    destinationQueuesReferenceData : [
        {
            destinationQueueId:1,
            destinationQueueName:"OG KDS"
        },{
            destinationQueueId:2,
            destinationQueueName:"OG KDS Salute"
        },{
            destinationQueueId:3,
            destinationQueueName:"OG KDS Tuscan"
        },{
            destinationQueueId:1,
            destinationQueueName:"OG KDSv Baskan"
        },{
            destinationQueueId:1,
            destinationQueueName:"OG KDS Maskan"
        },
    ]
}

