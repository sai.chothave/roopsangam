import { Button, Grid, Table } from '@mui/material';
import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom';
import { initialOptions } from './constant';


export const EDPDriver = () => {
    const navigate = useNavigate()

    const getEdpData = () => {
        return {
            delayPrintingInfoName: "Delay Printing Name__" + Math.floor(Math.random() * 10),
            orderSource: initialOptions.orderSourceReferenceData[Math.floor(Math.random() * initialOptions.orderSourceReferenceData.length)],
            orderChannel: initialOptions.orderChannelReferenceData[Math.floor(Math.random() * initialOptions.orderChannelReferenceData.length)],
            orderType: initialOptions.orderTypeReferenceData[Math.floor(Math.random() * initialOptions.orderTypeReferenceData.length)],
            printOption: initialOptions.printOptionReferenceData[Math.floor(Math.random() * initialOptions.printOptionReferenceData.length)],
            groupTicketPickupTimeFrame: Math.floor(Math.random() * 10),
            groupTicketOffsetTime: Math.floor(Math.random() * 10),
            kitchenOffsetTime: Math.floor(Math.random() * 10),
            destinationDeviceQueueId: initialOptions.destinationQueuesReferenceData[Math.floor(Math.random() * initialOptions.destinationQueuesReferenceData.length)],
            autoPrintingFlag: false,
        }
    }

    const [data, setData] = useState(getEdpData());

    const navigateToEDP = () => {
        navigate("/edp", { state: { ...data } })
    }

    return (
        <Grid container columns={1} sx={{ marginTop: "100px" }}>
            <Grid item md={1} display="flex" justifyContent={"center"} justifyItems={"center"}>
                <Button variant={"contained"} onClick={()=>navigateToEDP()}>Open EditDelayPrinting</Button>
            </Grid>
            <Grid container columns={5} sx={{ margin: "30px" }}>
                <Grid item md={1}>
                    delayPrintingInfoName
                </Grid>
                <Grid item md={1}>
                    orderSource
                </Grid>
                <Grid item md={1}>
                    orderChannel
                </Grid>
                <Grid item md={1}>
                    orderType
                </Grid>
                <Grid item md={1}>
                    printOption
                </Grid>

                <Grid item md={1}>
                    {data.delayPrintingInfoName}
                </Grid>
                <Grid item md={1}>
                    {data.orderSource.orderSourceName}
                </Grid>
                <Grid item md={1}>
                    {data.orderChannel.orderChannelName}
                </Grid>
                <Grid item md={1}>
                    {data.orderType.orderTypeName}
                </Grid>
                <Grid item md={1}>
                    {data.printOption.printOptionName}
                </Grid>
            </Grid>

        </Grid>
    )
}
