import React, { useEffect, useState, useCallback } from 'react'
import styles from './temp.module.scss';

export const Temp = () => {

    const [formdata, setFormData] = useState({fname:'',lname:''})
    const [formError, setFormError] = useState({fname:'',lname:''})
    const [touched, setTouched] = useState({fname:false,lname:false})

    const validate=(field,value)=>{
        if(value.length<=0)
            setFormError({...formError,[field]:"required"})
    }

    const handleInputChange = (e) =>{
        const value = e.target.value
        const name = e.target.name
        setFormData({...formdata,[name]:value})
        setTouched({...touched,[name]:true})
    }

    const handleSubmit = () => {
        for (const key in formdata) {
            validate(key,formdata[key])
        }
    }

    useEffect(()=>{
        console.log(formError)
    },[formError])

    return (
        <div>
            <div>
                <input name="fname" value={formdata.fname} onChange={handleInputChange}/>
                { formError.fname && <p>{formError.fname}</p> }
            </div><br/>
            <div>
                <input name="lname" value={formdata.lname} onChange={handleInputChange}/>
                { formError.lname && <p>{formError.lname}</p> }
            </div><br/>
                        
            <input type={"button"} onClick={handleSubmit} value="Submit"/>
        </div>
    )
}
