import { useContext } from 'react';
import { WindowContext } from '../../App';
import { BillingReport } from '../components/billingReport';
import DesktopNav from '../components/DesktopNav'
import MobileNav from '../components/MobileNav';
import ReportGeneratorDialog from '../components/ReportGenerator';
import { Temp } from './temp';



export const DashboardPage = () => {
  const winContext = useContext(WindowContext)

  const closeReports = () => {
    winContext?.setWindow(false)
  }

  return (
    <div>
      <DesktopNav />
      <MobileNav />
      <ReportGeneratorDialog open={winContext?.window === "reportWindow"} close={closeReports} />
      <BillingReport open={winContext?.window === "billingReportWindow"} close={closeReports} />
      {/* <Temp data-testid="temp"/> */}
    </div>
  )
}
