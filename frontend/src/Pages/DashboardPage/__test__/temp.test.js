import { render, screen, cleanup } from "@testing-library/react";
// Importing the jest testing library
import '@testing-library/jest-dom'

import { DashboardPage as Temp } from '../DashboardPage';

// afterEach function runs after each test suite is executed
afterEach(() => {
	cleanup(); // Resets the DOM after each test suite
})

describe("Temp Component" ,() => {
	const setToggle= jest.fn();
	render(<Temp />);
	const temp = screen.getByTestId("temp1");
	
	// Test 1
	test("Temp Rendering", () => {
		expect(temp).toBeInTheDocument();
	})

	// Test 2
	test("Temp Text", () => {
		expect(temp).toHaveTextContent("vgh");
	})
})
