import { IProduct, IProducts } from './interfaces/product'


export const getProducts = async (cat: string) => {

    // let category = ["Men's Ware", "Women's Ware"]
    let subCategory = ["Shirts", "Pants", "Garments", "Kids", "Night Wares"]
    let productNames = ["ABC Product", "DEF Product", "GHI Product", "JKL Product", "MNO Product", "PQR Product", "STU Product", "VWX Product", "YZ Product"]
    let productPrices = ["50", "100", "200", "300", "400", "500", "600", "700", "800", "900", "1000"]
    let productCodes = ["HEK", "ONI", "FBI", "OHC", "MRT", "WWR", "FDP"]
    let productSizes = ["S", "M", "L", "XL", "XXL", "3XL", "30", "32", "34", "36", "38"]



    let products: IProducts = {
        products: Array<IProduct>(),
        filters: Array<string>()
    };

    for (let i = 0; i < 100; i++) {
        let data: IProduct = {
            productName: productNames[Math.floor(Math.random() * productNames.length)],
            productCategory: cat,
            productSubCategory: subCategory[Math.floor(Math.random() * subCategory.length)],
            productPrice: Number(productPrices[Math.floor(Math.random() * subCategory.length)]),
            inStock: Math.floor(Math.random() * 10),
            productCode: productCodes[Math.floor(Math.random() * subCategory.length)],
            productSize: productSizes[Math.floor(Math.random() * productSizes.length)]
        }
        products.products.push(data)
    }

    products.filters = subCategory
    return new Promise<Array<IProduct>>((resolve) => {
        setTimeout(() => {
            resolve(products.products)
        }, 1000);
    }
    )

}
