import { DeleteOutlineRounded } from '@mui/icons-material';
import { Box, Button, Card, Grid, IconButton, InputAdornment, List, ListItem, ListItemAvatar, ListItemText, TextField, Typography } from '@mui/material';
import { width } from '@mui/system';
import { refType } from '@mui/utils';
import React, { useEffect, useState } from 'react'
import { IExpense } from './interfaces/expense';

const DailyExpense = () => {
    const [expenseList, setExpenseList] = useState<IExpense[]>([]);
    const [expense, setExpense] = useState<IExpense>({ reason: "", amount: 0 })
    const [total, setTotal] = useState(0);


    const addExpenseToList = () => {
        setExpenseList([...expenseList, expense])
    }

    const deleteExpenseFromList = (ind: Number) => {
        setExpenseList(expenseList.filter((exp, index) => index !== ind))
    }

    const countTotal = () => {
        setTotal(expenseList.reduce((pre, cur) => pre + cur.amount, 0))
    }

    useEffect(() => {
        // setExpense({ reason: "", amount: 0 })
        countTotal()
    }, [expenseList])

    return (
        <Box sx={{ display: 'flex', flexWrap: 'wrap' }} >
            {/* <Card sx={{ width: "100%", }} style={{ backgroundColor: "#0089ED", color: "white"}} elevation={2}> */}
            {/* <Grid container columns={2}>
                    <Grid item xs={1} sm={1} md={1} lg={1}>
                        <Typography sx={{ fontWeight: "600", textAlign: "left", margin: "10px" }}>Daily Expense</Typography>
                    </Grid>
                    <Grid item xs={1} sm={1} md={1} lg={1}>
                        <Typography sx={{ fontWeight: "600", textAlign: "right", margin: "10px" }}>Rs.{total}/-</Typography>
                    </Grid>
                </Grid> */}
            <Box sx={{ backgroundColor: "white", padding: "5px", margin: "5px" }}>
                <Grid container columns={10} spacing={{ md: 1, sm: 1, xs: 1 }} >
                    <Grid item xs={5} sm={5} md={5} lg={5}>
                        <TextField
                            label="Reason"
                            // value={expense.reason}
                            value={expense.reason}
                            id="ExpReason"
                            size='small'
                            onChange={(e) => {
                                setExpense({ ...expense, ["reason"]: e.target.value })
                            }}
                        />
                    </Grid>
                    <Grid item xs={3} sm={3} md={3} lg={3}>
                        <TextField
                            label={`Amount -- Rs.${total}/-`}
                            id="ExpAmount"
                            // value={expense.amount}
                            value={expense.amount}
                            type={"number"}
                            size='small'
                            InputProps={{
                                startAdornment: <InputAdornment position="start">Rs.</InputAdornment>,
                            }}
                            onChange={(e) => {
                                setExpense({ ...expense, ["amount"]: Number.parseInt(e.target.value) })
                            }}
                        />
                    </Grid>
                    <Grid item xs={2} sm={2} md={2} lg={2}>
                        <Button
                            disabled={(expense.amount !== 0) ? false : true}
                            onClick={addExpenseToList}
                        >Add</Button>
                    </Grid>
                </Grid>
            </Box>
            {/* </Card> */}
            <List sx={{ width: "100%", position: "relative" }}>
                {
                    expenseList?.map((exp, ind) => (
                        <ListItem key={ind}
                            secondaryAction={
                                <IconButton edge="end" aria-label="delete" onClick={() => deleteExpenseFromList(ind)}>
                                    <DeleteOutlineRounded />
                                </IconButton>
                            }
                        >
                            <ListItemText
                                primary={exp.reason}
                                secondary={exp.amount}
                            />
                        </ListItem>
                    ))
                }
            </List>
        </Box>
    )
}

export default DailyExpense