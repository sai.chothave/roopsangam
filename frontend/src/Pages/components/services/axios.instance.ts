import axios from "axios";

const BaseURL = 'http://localhost:8080'
const axiosInstance = axios.create({
    baseURL: BaseURL
});

axiosInstance.interceptors.request.use((config) => {
    const token = localStorage.getItem('authToken');
    config.headers = {
        Authorization: "Bearer "+token || ''
    }
    return config;
})

export default axiosInstance;