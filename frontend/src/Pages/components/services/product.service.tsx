import { NineteenMpTwoTone } from "@mui/icons-material";
import { IProduct } from "../interfaces/product";
import axios from "./axios.instance";


// export const login = async (credentials: ICredentials) => {
//     let response = undefined
//     try {
//         response = await axios.post('api/auth/login', credentials);
//         console.log(response)
//         return response;
//     } catch (e) {
//         console.log('something went wrong',response);
//         return {status:403}
//     }
// }

// export const register = async (user: IUser) => {
//     try {
//         console.log(user)
//         const response = await axios.post('api/auth/signup', user);
//         return response.data;
//     } catch (e) {
//         console.log('something went wrong Registration');
//         return e
//     }
// }

export const getAllProducts = () => {
    return axios.get("/product/")
}

export const getProductByCategory = (category:string) => {
    return axios.get("/product/getByCategory/"+category)
}

export const getValuesByFieldName = (fieldName:string) => {
    return axios.post("/product/valuesByFieldName",{fieldName:fieldName})
}

export const addProduct = (product:IProduct) => {
    return axios.post("/product",product)
}

export const updateProduct = (id:string,dataToUpdate:any) => {
    return axios.put("/product/"+id,dataToUpdate)
}

export const deleteProduct = (id:string) => {
    return axios.delete("/product/"+id)
}

export const getProductById = (id:string) => {
    return axios.get("/product/"+id)
}