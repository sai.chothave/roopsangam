import { IBill } from "../interfaces/bill";
import axios from "./axios.instance";



export const generateBill = (bill:IBill)=>{
    return axios.post("/billing/",bill)
}

export const getRecentNBills = (n:number) => {
    return axios.get("/billing/"+n)
}

export const getAllBills = () => {
    return axios.get("/billing")
}

export const updateBillStatus = (bill:IBill|undefined) => {
    return axios.patch("/billing/",bill)
}