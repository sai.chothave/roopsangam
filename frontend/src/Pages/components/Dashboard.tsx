// import { AddIcCallRounded, Grid3x3Rounded } from '@mui/icons-material'
import { Box, Card, Grid, List, ListItemButton, ListItemText, Skeleton, Typography } from '@mui/material'
import React, { useEffect } from 'react'
import { BarChart } from './BarChart'
import Clock from 'react-live-clock';
import BillingWindow from './BillingWindow';
import ReportGeneratorDialog from './ReportGenerator';
import PrintPreview from './PrintPreview';
import { IBill } from './interfaces/bill';
import DailyExpense from './DailyExpense';
import { getRecentNBills } from './services/billing.service';

export const Dashboard = () => {

    const labels = Array.from({ length: 30 }, (_, i) => i + 1)
    const values = Array.from({ length: 30 }, (_, i) => (i + 1) * (Math.floor(Math.random() * 10) * Math.floor(Math.random() * 10) * 10))
    // const orders = Array.from({ length: 30 }, (_, i) => { return { orderId: (i + 1), orderNumber: "Order Number" + (i + 1), orderAmount: "Rs.500" } })

    const [openBill, setOpenBill] = React.useState(false);
    const [myProductDialogOpen, setMyProductDialogOpen] = React.useState(false);
    const [bill, setBill] = React.useState<IBill>();
    const [orders,setOrders] = React.useState<IBill[]>();

    const handleListItemClick = (event: React.MouseEvent<HTMLDivElement, MouseEvent>, index: number,) => {
        let newBill: IBill = { billNumber: index, products: [{ productName: "ABC Product", inStock: 2, productCategory: "Men's Wear", productCode: "OHC", productPrice: 80, productSize: "34", productSubCategory: "Shirt", buyQuantity: 5 }], amount: 120, date: `${new Date().getDate()}/${new Date().getMonth() + 1}/${new Date().getFullYear()}`, time: `${new Date().toLocaleTimeString()}`, discount: 12, paymentMode: "Online" }
        setOpenBill(true)
        setBill(newBill)
    };

    useEffect(()=>{
        getRecentNBills(29).then((bills)=>{
            if(bills){
                setOrders(bills.data)
            }
        }).catch((err)=>{
            console.log("Error while fetching Bills => ",err)
        })
    },[])

    return (
        <div>
            <Grid container spacing={{ xs: 1, sm: 1, md: 1 }} columns={{ xs: 1, sm: 5, md: 5 }}>
                <Grid item xs={1} sm={3} md={3} order={{ xs: 2, md: 1, sm: 1 }}>
                    <Card sx={{ width: "100%", maxWidth: "800px", height: "350px", textAlign: "center", backgroundColor: "#fff7f0" }} elevation={3}>
                        <Typography variant='caption'> Monthly Data - June </Typography>
                        <BarChart labels={labels} data={values} heading="" month="June" height="300px" />
                    </Card>
                    <Card sx={{ marginTop: "10px", height: "260px", overflowY: "scroll", border: "2px solid #0089ED" }}>
                        <Box sx={{ position: "relative" }}>
                            <Card sx={{ margin: "-1px", position: "relative", width: "100%", }} style={{ backgroundColor: "#0089ED", color: "white" }} elevation={2}>
                                <Typography sx={{ fontWeight: "600", textAlign: "left", margin: "10px" }}>Recent Orders</Typography>
                            </Card>
                        </Box>

                        <List component="nav" aria-label="main mailbox folders" sx={{}}>
                            <Grid container spacing={1} columns={{ xs: 2, sm: 2, md: 4, lg: 4 }}>
                                {
                                    orders && orders.map((order: any, ind: number) =>
                                        <Grid item md={1} xs={1} sm={1} lg={1} key={ind}>
                                            <ListItemButton key={ind} onClick={(event: any) => handleListItemClick(event, ind)}>
                                                <ListItemText primary={order.orderNumber} />
                                            </ListItemButton>
                                        </Grid>
                                    )
                                }
                            </Grid>
                        </List>
                    </Card>
                </Grid>
                <Grid item xs={1} sm={2} md={2} order={{ xs: 1, sm: 1, md: 1 }}>
                    {/* #f0f9ff */}
                    <Card sx={{ width: "100%", maxWidth: "800px", height: "100%", textAlign: "center", zIndex: "100", position: "relative" }} elevation={0}>
                        <Card sx={{ marginBottom: "-3px", marginTop: { xs: "15px", md: "0px" }, borderRadius: { xs: "30px 30px 0px 0px", md: "0px" } }} style={{ backgroundColor: "#0089ED", color: "white" }} elevation={1}>
                            <Typography sx={{ fontWeight: "600", textAlign: "left", margin: "20px 0px 0px 10px" }}> {` ${new Date().toDateString()} `} </Typography>
                            <Typography sx={{ fontWeight: "600", textAlign: "left", margin: "0px 0px 10px 15px" }} variant="body2">
                                <Clock format={'HH:mm:ss'} ticking={true} timezone={'Asia/Calcutta'} />
                            </Typography>
                        </Card>
                        <Card sx={{ border: "2px solid #0089ED" }}>
                            <Grid container spacing={{ xs: 1, sm: 1, md: 1 }} columns={{ xs: 2, sm: 2, md: 2 }} padding={1}>
                                <Grid item xs={1} sm={1} md={1} padding={1}>
                                    <Card style={{ backgroundColor: "#0089ED", color: "white" }} onClick={()=>{window.location.replace("/scanner")}}>
                                        <Typography variant='h6'>Mens Wear</Typography>
                                        <Typography>365 Varieties</Typography>
                                    </Card>
                                </Grid>
                                <Grid item xs={1} sm={1} md={1}>
                                    <Card style={{ backgroundColor: "#e04848", color: "white" }}>
                                        <Typography variant='h6'>Womens Wear</Typography>
                                        <Typography>241 Varieties</Typography>
                                    </Card>
                                </Grid>
                                <Grid item xs={1} sm={1} md={1} padding={1}>
                                    <Card style={{ backgroundColor: "#e04848", color: "white" }}>
                                        <Typography variant='h6'>Today's</Typography>
                                        <Typography>Rs.15231/-</Typography>
                                    </Card>
                                </Grid>
                                <Grid item xs={1} sm={1} md={1}>
                                    <Card style={{ backgroundColor: "#0089ED", color: "white", cursor: "pointer" }} onClick={() => setMyProductDialogOpen(true)}>
                                        <Typography variant='h6'>My Products</Typography>
                                        <Typography>Generate Reports</Typography>
                                    </Card>
                                </Grid>
                            </Grid>
                        </Card>
                        <Card sx={{ marginTop: "10px", height: "380px", overflowY: "scroll", border: "2px solid #0089ED" }}>
                            <DailyExpense />
                        </Card>
                    </Card>
                </Grid>
            </Grid>
            <BillingWindow />
            {
                myProductDialogOpen && <ReportGeneratorDialog open={myProductDialogOpen} setOpen={setMyProductDialogOpen} />
            }
            {
                openBill && <PrintPreview bill={bill!} open={openBill} onClose={() => setOpenBill(false)} />
            }
        </div>
    )
}
