import * as React from 'react';
import DialogTitle from '@mui/material/DialogTitle';
import Dialog from '@mui/material/Dialog';
import Typography from '@mui/material/Typography';
import { Box, Divider, Grid } from '@mui/material';
import { IPurchasedProducts } from './interfaces/product';
import { IBill } from './interfaces/bill';

// export interface PrintPreviewDialog {
//     open: boolean;
//     onClose: () => void;
//     products: IPurchasedProducts[];
//     // amount?: number;
//     discount: number | number[];
//     // totalProducts?:number;
// }

export interface PrintPreviewDialog {
    open: boolean;
    onClose: () => void;
    bill : IBill
}

export default function PrintPreview(props: PrintPreviewDialog) {
    const { onClose, open } = props;
    const { billNumber, products, paymentMode, date, time, amount, discount  } = props.bill;
    // const [amount , setAmount] = React.useState<number>(0);
    const [totalProducts, setTotalProducts] = React.useState<number>(0);

    const handleClose = () => {
        onClose();
    };

    React.useEffect(()=>{
        // setAmount(products?.reduce((total, prod) => { return total + (prod.productPrice * prod.buyQuantity!) }, 0))
        setTotalProducts(products?.reduce((total, prod) => { return total + prod.buyQuantity! }, 0))
    },[products])



    return (
        <Dialog open={open} sx={{ width: "4inches" }} scroll="paper" title='Tap on bill to close'>
            {/* <IconButton
                aria-label="close"
                color="inherit"
                size="small"
                onClick={() => {
                    handleClose()
                }}
            >
                <CloseIcon fontSize="inherit" />
            </IconButton> */}
            <DialogTitle sx={{ textAlign: "center", fontSize: "16pt" }}>Invoice</DialogTitle>
            <Box sx={{ textAlign: "center", p: 1 }} onClick={handleClose}>
                <Typography fontSize={"16pt"}>
                    Roopsangam Dresses
                </Typography>
                <Grid container direction="row" alignItems="center" justifyContent="center" fontSize={"10pt"} columns={3} spacing={1}>
                    <Grid item>&#9733; Men's Wear</Grid>
                    <Grid item>&#9733; Ladies Wear</Grid>
                    <Grid item>&#9733; hojari</Grid>
                </Grid>
                <Typography fontSize={"10pt"}>
                    Shop No. 15,16,<br />
                    Nagarpanchyat Complex,<br />
                    Shirdi, Maharashtra - 423109
                </Typography>

                <Divider sx={{ margin: "2px", mt: 1, mb: 1 }} />

                <Grid container columns={2}>
                    <Grid item xs={1} md={1} lg={1}>
                        <Typography fontSize={"10pt"} textAlign={"left"}>
                            BILL NO : { billNumber } <br />
                            {/* {`DATE : ${new Date().getDate()}/${new Date().getMonth() + 1}/${new Date().getFullYear()}`} */}
                            DATE : {`${new Date(date).getDate()}/${new Date(date).getMonth() + 1}/${new Date(date).getFullYear()}`}
                        </Typography>
                    </Grid>
                    <Grid item xs={1} md={1} lg={1}>
                        <Typography fontSize={"10pt"} textAlign={"right"}>
                            {/* {`TIME : ${new Date().toLocaleTimeString()}`} */}
                            TIME : {time}
                        </Typography>
                    </Grid>
                </Grid>

                <Divider sx={{ margin: "2px", mt: 1 }} />

                <div className="Table">
                    <Grid container columns={12} fontSize={"10pt"} textAlign={"left"} className="TH-row">
                        <Grid item md={1} sm={1} xs={1} lg={1} > SI </Grid>
                        <Grid item md={6} sm={6} xs={6} lg={6}> ITEM NAME </Grid>
                        <Grid item md={2} sm={2} xs={2} lg={2}> Qty </Grid>
                        <Grid item md={3} sm={3} xs={3} lg={3}> AMOUNT </Grid>
                    </Grid>

                    <Divider sx={{ margin: "2px" }} />

                    {
                        products && products.map((prod: IPurchasedProducts, ind) =>
                            <Grid key={ind} container columns={12} fontSize={"9pt"} textAlign={"left"} className="TR-row">
                                <Grid item md={1} sm={1} xs={1} lg={1}> {ind + 1} </Grid>
                                <Grid item md={6} sm={6} xs={6} lg={6}><Typography fontSize={"inherit"} noWrap>{prod.productName} - {prod.productSize}</Typography></Grid>
                                <Grid item md={2} sm={2} xs={2} lg={2}> {prod.buyQuantity} </Grid>
                                <Grid item md={3} sm={3} xs={3} lg={3}> {prod.productPrice * prod.buyQuantity!} </Grid>
                            </Grid>
                        )
                    }

                </div>

                <Divider sx={{ mt: "5px" }} />

                <Grid container columns={2}>
                    <Grid item xs={1} md={1} lg={1}>
                        <Typography fontSize={(discount! > 0) ? "8pt" : "10pt"} sx={{ fontWeight: "bolder" }} textAlign={"left"}>Total Items : {totalProducts}</Typography>
                    </Grid>
                    <Grid item xs={1} md={1} lg={1}>
                        <Typography fontSize={(discount! > 0) ? "8pt" : "10pt"} sx={{ fontWeight: "bolder" }} textAlign={"right"}>TOTAL : &#8377; {amount}</Typography>
                    </Grid>
                </Grid>
                {
                    (discount! > 0) ?
                        <Grid container columns={2}>
                            <Grid item xs={1} md={1} lg={1}>
                                <Typography fontSize={"10pt"} sx={{ fontWeight: "bolder" }} textAlign={"left"}>Discount : {discount}%</Typography>
                            </Grid>
                            <Grid item xs={1} md={1} lg={1}>
                                <Typography fontSize={"10pt"} sx={{ fontWeight: "bolder" }} textAlign={"right"}>Net Amt : {(paymentMode==="Online")?<span>&#8377; </span>:<span>&#8377; </span>}{(amount! - ((Number(discount) / 100) * amount!)).toFixed(0)}</Typography>
                            </Grid>
                        </Grid> : <div></div>
                }


                <Divider sx={{ mb: "2px" }} />

                <Typography fontSize={"10pt"}>
                    Thank You... Visit Again...&#128515;
                </Typography>

            </Box>
            {/* <List sx={{ pt: 0 }}>
                {emails.map((email) => (
                    <ListItem button onClick={() => handleListItemClick(email)} key={email}>
                        <ListItemAvatar>
                            <Avatar sx={{ bgcolor: blue[100], color: blue[600] }}>
                                <PersonIcon />
                            </Avatar>
                        </ListItemAvatar>
                        <ListItemText primary={email} />
                    </ListItem>
                ))}
                <ListItem autoFocus button onClick={() => handleListItemClick('addAccount')}>
                    <ListItemAvatar>
                        <Avatar>
                            <AddIcon />
                        </Avatar>
                    </ListItemAvatar>
                    <ListItemText primary="Add account" />
                </ListItem>
            </List> */}
        </Dialog>
    );
}