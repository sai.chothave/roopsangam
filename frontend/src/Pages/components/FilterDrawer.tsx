import * as React from 'react';
import { Global } from '@emotion/react';
import { styled } from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';
import { grey } from '@mui/material/colors';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import SwipeableDrawer from '@mui/material/SwipeableDrawer';
import { List, ListItem, ListItemButton, ListItemText, ListSubheader, Switch } from '@mui/material';

const drawerBleeding = 0;

// interface Props {
//     /**
//      * Injected by the documentation to work in an iframe.
//      * You won't need it on your project.
//      */
//     window?: () => Window;
// }

const Root = styled('div')(({ theme }) => ({
    height: '100%',
    backgroundColor:
        theme.palette.mode === 'light' ? grey[100] : theme.palette.background.default,
}));

const StyledBox = styled(Box)(({ theme }) => ({
    backgroundColor: theme.palette.mode === 'light' ? '#fff' : grey[800],
}));

const Puller = styled(Box)(({ theme }) => ({
    width: 30,
    height: 6,
    backgroundColor: theme.palette.mode === 'light' ? grey[300] : grey[900],
    borderRadius: 3,
    position: 'absolute',
    top: 8,
    left: 'calc(50% - 15px)',
}));

export default function FilterDrawer(props: any) {
    const { window, addFilters,filters  } = props;
    // const [open, setOpen] = React.useState(false);

    const toggleDrawer = (newOpen: boolean) => () => {
        // setOpen(newOpen);
        props.setFilterOpen(newOpen)
    };

    // This is used only for the example
    const container = window !== undefined ? () => window().document.body : undefined;

    const handleToggle = (filter: string) => {
        const currentFilter = props.filters.indexOf(filter);
        const newFilter = [...props.filters];

        if (currentFilter === -1) {
            newFilter.push(filter);
        } else {
            newFilter.splice(currentFilter, 1);
        }
        props.setFilters(newFilter)
    }

    React.useEffect(() => {
        addFilters()
    }, [filters])

    return (
        <Root>
            <CssBaseline />
            <Global
                styles={{
                    '.MuiDrawer-root > .MuiPaper-root': {
                        height: `calc(50% - ${50}px)`,
                        overflow: 'visible',
                    },
                }}
            />

            <SwipeableDrawer
                container={container}
                anchor="bottom"
                open={props.filterOpen}
                onClose={toggleDrawer(false)}
                onOpen={toggleDrawer(true)}
                swipeAreaWidth={drawerBleeding}
                disableSwipeToOpen={false}
                ModalProps={{
                    keepMounted: true,
                }}
            >
                <StyledBox
                    sx={{
                        position: 'absolute',
                        top: -drawerBleeding,
                        borderTopLeftRadius: 8,
                        borderTopRightRadius: 8,
                        visibility: 'visible',
                        right: 0,
                        left: 0,
                    }}
                >
                    <Puller />
                    <Typography sx={{ p: 2, color: 'text.secondary' }}>{`${props.totalResult} results`}</Typography>
                </StyledBox>
                <StyledBox
                    sx={{
                        px: 2,
                        pb: 2,
                        height: '100%',
                        overflow: 'auto',
                    }}
                >
                    <List
                        sx={{ width: '100%', bgcolor: 'background.paper', marginTop: "45px" }}
                        subheader={<ListSubheader>Filters</ListSubheader>}
                    >
                        {
                            props.availableFilters.map((filter: string) =>
                                <ListItem disablePadding key={filter}>
                                    <ListItemButton role={undefined} onClick={() => handleToggle(filter)} dense>
                                        <ListItemText id={`filter-${filter}`} primary={filter} />
                                        <Switch aria-label={filter} checked={props.filters.indexOf(filter) !== -1} edge="end" name={filter} inputProps={{ 'aria-labelledby': "Filter" }} />
                                    </ListItemButton>
                                </ListItem>
                            )
                        }
                    </List>
                    {/* <Button onClick={() => props.setFilters(["Shirts"])}>Add Shirts Filter</Button> */}
                </StyledBox>
            </SwipeableDrawer>
        </Root>
    );
}
