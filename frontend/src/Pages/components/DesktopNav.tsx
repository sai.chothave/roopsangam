import * as React from 'react';
import { styled, useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Drawer from '@mui/material/Drawer';
import CssBaseline from '@mui/material/CssBaseline';
import MuiAppBar, { AppBarProps as MuiAppBarProps } from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import List from '@mui/material/List';
import Typography from '@mui/material/Typography';
import Divider from '@mui/material/Divider';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import LogoutRoundedIcon from '@mui/icons-material/LogoutRounded';
import { ProductCard } from './ProductCard';
import { Tabs } from './constants/Tabs';
import { Dashboard } from './Dashboard';
import { ModernDashboard } from './Modern Dashboard/ModernDashboard';
import { Backdrop, Button, Card, Chip, CircularProgress, Paper, Table, TableBody, TableContainer, TableHead, TableRow } from '@mui/material';
import AddProduct from './AddProduct';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';

const drawerWidth = 240;

const Main = styled('main', { shouldForwardProp: (prop) => prop !== 'open' })<{
    open?: boolean;
}>(({ theme, open }) => ({
    flexGrow: 1,
    padding: theme.spacing(3),
    transition: theme.transitions.create('margin', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
    }),
    marginLeft: `-${drawerWidth}px`,
    ...(open && {
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
        marginLeft: 0,
    }),
}));

const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
        backgroundColor: "#85a3e0",
        color: theme.palette.common.white,
    },
    [`&.${tableCellClasses.body}`]: {
        fontSize: 14,
        alignText: "center",
    },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
    },
    // hide last border
    '&:last-child td, &:last-child th': {
        border: 0,
    },
}));

interface AppBarProps extends MuiAppBarProps {
    open?: boolean;
}

const AppBar = styled(MuiAppBar, {
    shouldForwardProp: (prop) => prop !== 'open',
})<AppBarProps>(({ theme, open }) => ({
    transition: theme.transitions.create(['margin', 'width'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
    }),
    ...(open && {
        width: `calc(100% - ${drawerWidth}px)`,
        marginLeft: `${drawerWidth}px`,
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
    }),
}));

const DrawerHeader = styled('div')(({ theme }) => ({
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
    justifyContent: 'flex-end',
}));

export default function DesktopNav() {
    const theme = useTheme();
    const [open, setOpen] = React.useState(false);
    const navTabs = Tabs();
    const [activeCategory, setActiveCategory] = React.useState<string>("Dashboard")
    const [openAddProduct, setOpenAddProduct] = React.useState(false);
    const [backdrop, setBackdrop] = React.useState(true)

    const handleDrawerOpen = () => {
        setOpen(true);
    };

    const handleDrawerClose = () => {
        setOpen(false);
    };


    React.useEffect(() => {
        setTimeout(() => {
            handleDrawerOpen()
            setBackdrop(false)
        }, 1000)
    }, [activeCategory])

    return (
        <Box sx={{ display: { xs: 'none', md: 'flex' } }}>
            <CssBaseline />
            <AppBar position="fixed" open={open} sx={{ backgroundColor: "#0089ED" }}>
                <Toolbar>
                    <IconButton
                        color="inherit"
                        aria-label="open drawer"
                        onClick={handleDrawerOpen}
                        edge="start"
                        sx={{ mr: 2, ...(open && { display: 'none' }) }}
                    >
                        <MenuIcon />
                    </IconButton>
                    <Typography variant="h6" noWrap component="div">
                        Roopsangam
                    </Typography>
                </Toolbar>
                {
                    (activeCategory.toLocaleLowerCase().indexOf("wear") >= 0) ? <Button variant='text' onClick={() => setOpenAddProduct(true)} sx={{ color: "white", position: "fixed", top: "15px", right: "10px" }}>Add Product</Button> : <></>
                }
                {
                    openAddProduct && <AddProduct open={openAddProduct} setOpen={setOpenAddProduct} />
                }

            </AppBar>
            <Drawer
                sx={{
                    width: drawerWidth,
                    flexShrink: 0,
                    '& .MuiDrawer-paper': {
                        width: drawerWidth,
                        boxSizing: 'border-box',
                    }
                }}
                variant="persistent"
                anchor="left"
                open={open}
            >
                <DrawerHeader>
                    <IconButton onClick={handleDrawerClose}>
                        {theme.direction === 'ltr' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
                    </IconButton>
                </DrawerHeader>
                <Divider />
                <List>

                    {
                        navTabs.map((tab: any, ind: number) =>
                            <ListItem disablePadding key={ind} selected={(activeCategory === tab.name) ? true : false} onClick={() => { setActiveCategory(tab.name) }}>
                                <ListItemButton>
                                    <ListItemIcon>
                                        {tab.icon}
                                    </ListItemIcon>
                                    <ListItemText primary={tab.displayName} />
                                </ListItemButton>
                            </ListItem>
                        )
                    }
                </List>
                <Divider />
                <List>
                    <ListItem disablePadding>
                        <ListItemButton>
                            <ListItemIcon>
                                <LogoutRoundedIcon />
                            </ListItemIcon>
                            <ListItemText primary={"Logout"} />
                        </ListItemButton>
                    </ListItem>
                </List>
                <Divider />
                <List>
                    <TableContainer component={Paper} sx={{ marginTop: "50px", width: "220px", mr: 1, ml: 1 }}>
                        <Table size="small">
                            <TableHead>
                                <StyledTableRow>
                                    <StyledTableCell>Action</StyledTableCell>
                                    <StyledTableCell>Command</StyledTableCell>
                                </StyledTableRow>
                            </TableHead>
                            <TableBody>
                                <StyledTableRow>
                                    <StyledTableCell>Billing Window</StyledTableCell>
                                    <StyledTableCell>Ctrl + B</StyledTableCell>
                                </StyledTableRow>
                                <StyledTableRow>
                                    <StyledTableCell>Add Product</StyledTableCell>
                                    <StyledTableCell>Shift + P</StyledTableCell>
                                </StyledTableRow>
                                <StyledTableRow>
                                    <StyledTableCell>All Bills</StyledTableCell>
                                    <StyledTableCell>Shift + B</StyledTableCell>
                                </StyledTableRow>
                                <StyledTableRow>
                                    <StyledTableCell>All Products</StyledTableCell>
                                    <StyledTableCell>Shift + R</StyledTableCell>
                                </StyledTableRow>
                            </TableBody>
                        </Table>
                    </TableContainer>
                </List>
            </Drawer>
            <Main open={open} sx={{ backgroundColor: "#eff4f7" }}>
                <DrawerHeader />
                {
                    (activeCategory.toLocaleLowerCase().indexOf("wear") >= 0) ? <ProductCard category={activeCategory.toLocaleUpperCase()} /> : (activeCategory.toLocaleLowerCase() === "dashboard") ? <ModernDashboard /> : <></>
                }
            </Main>
            <Backdrop
                sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
                open={backdrop}
                onClick={() => { }}
            >
                <CircularProgress color="inherit" />
            </Backdrop>
        </Box>
    );
}
