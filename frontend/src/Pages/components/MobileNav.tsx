import * as React from 'react';
import Box from '@mui/material/Box';
import CssBaseline from '@mui/material/CssBaseline';
import BottomNavigation from '@mui/material/BottomNavigation';
import BottomNavigationAction from '@mui/material/BottomNavigationAction';
import Paper from '@mui/material/Paper';
import { AppBar, IconButton, Toolbar, Typography } from '@mui/material';
import { ProductCard } from './ProductCard';

import { Tabs } from './constants/Tabs';
import LogoutIcon from '@mui/icons-material/Logout';
import { Dashboard } from './Dashboard';
import corner from "../../asset/corner.svg"

// function refreshMessages(): MessageExample[] {
//     const getRandomInt = (max: number) => Math.floor(Math.random() * Math.floor(max));


//     return Array.from(new Array(50)).map(
//         () => messageExamples[getRandomInt(messageExamples.length)],
//     );


// }

export default function MobileNav() {
    const [value, setValue] = React.useState(0);
    const ref = React.useRef<HTMLDivElement>(null);
    // const [messages, setMessages] = React.useState(() => refreshMessages());
    const [activeCategory, setActiveCategory] = React.useState<string>("Dashboard")

    const navTabs = Tabs()

    React.useEffect(() => {
        (ref.current as HTMLDivElement).ownerDocument.body.scrollTop = 0;
        // setMessages(refreshMessages());
    }, [value,activeCategory]);

    return (
        <Box sx={{ pb: 7, display: { xs: 'block', md: 'none' }}} ref={ref}>
            <AppBar elevation={2} sx={{backgroundColor:"#0089ED"}}>
                <Toolbar>
                    <Typography variant="h6" component="div">
                        Roopsangam
                    </Typography>
                    <div>
                        <IconButton
                            size="large"
                            aria-label="account of current user"
                            aria-controls="menu-appbar"
                            aria-haspopup="true"
                            onClick={() => { }}
                            color="inherit"
                            sx={{ position: "absolute", right: "10px", top: "5px" }}
                        >
                            <LogoutIcon />
                        </IconButton>
                    </div>
                    
                </Toolbar>
                <img src={corner} alt="left corner" style={{height:"30px",width:"30px",position:"absolute", top:"56px", left:"0px"}} />
                <img src={corner} alt="right corner" style={{height:"30px",width:"30px",position:"absolute", top:"56px", right:"0px", transform:"scaleX(-1)"}} />
                {/* <Box sx={{backgroundColor:"#0089ED",height:"30px",width:"30px",position:"absolute", top:"56px", left:"0px"}} >
                    <Box sx={{backgroundColor:"white",height:"30px",width:"30px",borderRadius:"30px 0px 0px 0px"}}>
                    </Box>
                </Box> */}
                {/* <Box sx={{backgroundColor:"#0089ED",height:"30px",width:"30px",position:"absolute", top:"56px", right:"0px"}}>
                    <Box sx={{backgroundColor:"white",height:"30px",width:"30px",borderRadius:"00px 30px 0px 0px"}}>
                    </Box>
                </Box> */}
            </AppBar>
            

            <CssBaseline />

            {/* <List sx={{marginTop:"50px"}}>
                {messages.map(({ primary, secondary, person }, index) => (
                    <ListItem button key={index + person}>
                        <ListItemAvatar>
                            <Avatar alt="Profile Picture" src={person} />
                        </ListItemAvatar>
                        <ListItemText primary={primary} secondary={secondary} />
                    </ListItem>
                ))}
            </List> */}
            <Paper sx={{paddingLeft: { xs: 2 }, paddingRight: { xs: 2 }, marginTop: { xs: "64px" }, marginBottom: { xs: "10px" }, zIndex:"10" }} elevation={0}>
                {
                    (activeCategory.toLocaleLowerCase().indexOf("wear")>=0)?<ProductCard category={activeCategory.toLocaleUpperCase()} />:(activeCategory.toLocaleLowerCase()==="dashboard")?<Dashboard/>:<></>
                }
            </Paper>

            <Paper sx={{ position: 'fixed', bottom: 0, left: 0, right: 0 }} elevation={10}>
                <BottomNavigation
                    showLabels
                    value={value}
                    onChange={(event, newValue) => {
                        setValue(newValue);
                        setActiveCategory(navTabs[newValue].name)
                    }}
                >
                    {
                        navTabs.map((tab: any, ind: number) =>
                            <BottomNavigationAction key={ind} label={tab.displayName} icon={tab.icon} />
                        )
                    }
                </BottomNavigation>
            </Paper>
        </Box>
    );
}
