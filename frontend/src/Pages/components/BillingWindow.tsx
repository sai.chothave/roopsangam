import * as React from 'react';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import ListItemText from '@mui/material/ListItemText';
import ListItem from '@mui/material/ListItem';
import List from '@mui/material/List';
import Divider from '@mui/material/Divider';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import CloseIcon from '@mui/icons-material/Close';
import Slide from '@mui/material/Slide';
import { TransitionProps } from '@mui/material/transitions';
import AddShoppingCartOutlinedIcon from '@mui/icons-material/AddShoppingCartOutlined';
import { Avatar, Box, Card, Chip, Fab, Grid, ListItemAvatar, Skeleton, Slider, ToggleButton, ToggleButtonGroup } from '@mui/material';
import { IPurchasedProducts } from './interfaces/product';


import Checkbox from '@mui/material/Checkbox';
import TextField from '@mui/material/TextField';
import Autocomplete, { AutocompleteOwnerState, AutocompleteRenderGetTagProps } from '@mui/material/Autocomplete';
import CheckBoxOutlineBlankIcon from '@mui/icons-material/CheckBoxOutlineBlank';
import CheckBoxIcon from '@mui/icons-material/CheckBox';
import { getProducts } from './dataFetcher';
import PrintPreview from './PrintPreview';

import AddIcon from '@mui/icons-material/Add';
import RemoveIcon from '@mui/icons-material/Remove';

import PopupAlert from './PopupAlert';
import { IBill } from './interfaces/bill';
import { getAllProducts, getProductById } from './services/product.service';
import { generateBill } from './services/billing.service';
import { chain, countBy, forEach, groupBy, keyBy } from 'lodash';
import { socketContext, WindowContext } from '../../App';
import { ProductCard } from './ProductCard';
import { OptionUnstyled } from '@mui/base';

const icon = <CheckBoxOutlineBlankIcon fontSize="small" />;
const checkedIcon = <CheckBoxIcon fontSize="small" />;

export function SearchProduct(props: any) {
    // const [allProducts, setAllProducts] = React.useState<Array<IPurchasedProducts>>([]);

    // React.useEffect(() => {
    //     // getProductByCategory(props.category)?.then((prod) => {
    //     //     console.log(prod)
    //     //     setProducts(prod.data)
    //     //     setFilteredProducts(prod.data)
    //     // })

    //     // getProducts("All")?.then((prod) => {
    //     //     setAllProducts(prod)
    //     // })

    //     getAllProducts().then((result) => {
    //         setAllProducts(result.data)
    //     }).catch((err) => {
    //         alert("Unable to fetch Products")
    //     })
    // }, [])


    return (
        <Autocomplete
            multiple
            id="SelectPurchasedProducts"
            options={props.allProducts}
            limitTags={1}
            onChange={(event: React.SyntheticEvent, value: IPurchasedProducts[], reson: string, details: any) => {
                // console.log(details.option)
                // value.map(prod => prod.buyQuantity = 1)
                if (reson === "removeOption") {
                    props.setSelectedProducts((prev: IPurchasedProducts[]) => prev.filter((p: IPurchasedProducts) => p._id !== details.option._id))
                } else if (reson === "selectOption" && details.option.inStock>0) {
                    details.option.buyQuantity = 1
                    props.setSelectedProducts((prev:IPurchasedProducts[])=>[...prev, details.option])
                }
            }}
            getOptionLabel={(option: IPurchasedProducts) => option.productName}
            renderOption={(p, option, { selected }) => (
                <li {...p} key={`${option.productName}-${option.productCode}-${option.productSize}-${option.inStock}`}>
                    <Checkbox icon={icon} checkedIcon={checkedIcon} style={{ marginRight: 8 }} checked={selected} disabled={option.inStock==0} />
                    <Grid container>
                        <Grid>
                            {option.productName}
                        </Grid>
                        <Grid container>
                            <Grid item sx={{ width: "50%" }}>
                                <Typography variant="subtitle1" color="text.secondary" component="div">
                                    {option.productCategory}
                                </Typography>
                            </Grid>
                            <Grid item sx={{ width: "50%" }}>
                                <Chip label={option.productSize} color="primary" variant="outlined" size="small" sx={{ right: "0px" }} />
                            </Grid>
                            {/* <Grid item sx={{width: "30%"}}>
                                <Typography variant="subtitle1" color="primary.success" component="div">
                                    <span style={{ color: "green" }}>{`Rs. ${option.productPrice}/-`}</span>
                                </Typography>
                            </Grid> */}
                        </Grid>
                    </Grid>
                </li>
            )}
            style={{ width: "100%" }}
            renderInput={(params) => (
                <TextField {...params} label="Select Products" autoFocus={true} variant='outlined' placeholder="Products" sx={{ position: "fixed", backgroundColor: "white", bottom: { xs: "10px", md: "30px", lg: "30px" }, marginLeft: { xs: "3%", md: "25%", lg: "25%" }, width: { xs: "94%", md: "50%", lg: "50%" } }} />
            )}

        // renderTags={(value,getTagProps, ownerState)=>{
        //    return value.map((prod,ind)=>(props.purchasedProducts.filter((p:any)=>p._id===prod._id).length>0)&&<Chip key={prod._id} label={prod.productName}/>)
        // }}
        />
    );
}


const Transition = React.forwardRef(function Transition(
    props: TransitionProps & {
        children: React.ReactElement;
    },
    ref: React.Ref<unknown>,
) {
    return <Slide direction="up" ref={ref} {...props} />;
});

export default function BillingWindow() {
    const [allProducts, setAllProducts] = React.useState<Array<IPurchasedProducts>>();

    const [open, setOpen] = React.useState(false);
    const [purchasedProducts, setPurchasedProducts] = React.useState<Array<IPurchasedProducts>>([]);
    const [purchasedProductsConc, setPurchasedProductsConc] = React.useState<Array<IPurchasedProducts>>([]);
    const [billAmount, setBillAmount] = React.useState<number>(0);
    const [printPreview, setPrintPreview] = React.useState(false);
    const [discount, setDiscount] = React.useState<number | number[]>(0);
    const [error, setError] = React.useState<{ type: string, msg: string }>();
    const [paymentMode, setPaymentMode] = React.useState<"Online" | "Cash" | null>(null);
    const [bill, setBill] = React.useState<IBill>();

    const winContext = React.useContext(WindowContext);
    const socket = React.useContext(socketContext);

    const resetState = React.useCallback(() => {
        console.log("Billing Window Reseted")
        setBillAmount(0);
        setPurchasedProducts([]);
        setPaymentMode(null);
        setDiscount(0);
    }, [open, winContext?.window])

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        resetState()
        setOpen(false);
        winContext?.setWindow(false)
        socket.emit("billingWindowClosed")
    };

    const handleSaveAndPrint = () => {
        if (purchasedProducts.length === 0) {
            setError({ type: "error", msg: "Please add atleast one product to generate bill" });
        }
        else if (paymentMode === null)
            setError({ type: "error", msg: "Please select payment mode" })
        else {
            let billedProd = purchasedProductsConc.map((prod) => { return { product: prod._id, ...prod } })
            let newBill: IBill = { billNumber: 1, products: billedProd, amount: billAmount, date: `${new Date().getMonth() + 1}/${new Date().getDate()}/${new Date().getFullYear()}`, time: `${new Date().toLocaleTimeString()}`, discount: discount, paymentMode: paymentMode }
            console.log("Bill", newBill)
            generateBill(newBill).then((bill) => {
                setPrintPreview(true);
                handleClose();
                setBill(bill.data)
                socket.emit("acceptOnlinePayment", bill.data)
            }).catch((err) => {
                console.log(err)
                setError({ type: "error", msg: "Error => " + err.response.data })
            })
        }
    }

    const handlePrintPreviewClose = () => {
        setPrintPreview(false)
    }

    const concatPurProduct = React.useCallback(() => {
        const res = groupBy(purchasedProducts, (prod) => prod._id)
        let purProd: any = [];
        for (const id in res) {
            purProd.push({ ...res[id][0], buyQuantity: res[id].length })
        }
        setPurchasedProductsConc(purProd)
    }, [purchasedProducts])

    React.useEffect(() => {
        setBillAmount(purchasedProducts?.reduce((total, prod) => { return total + (prod.productPrice * prod.buyQuantity!) }, 0))
        concatPurProduct()
        if (purchasedProducts.length > 0)
            socket.emit("productAdded", [...purchasedProducts])
        // setTotalProducts(purchasedProducts?.reduce((total, prod) => { return total + prod.buyQuantity! }, 0))
    }, [purchasedProducts])

    const isAvailable = React.useCallback((prod: any) => {
        console.log(purchasedProducts)
        return purchasedProducts.map(p => p._id).indexOf(prod._id)
    }, [purchasedProducts])

    React.useEffect(() => {
        if (open || winContext?.window === 'billingWindow') {
            socket.emit("billingWindowOpened")
        } else {
            socket.emit("billingWindowClosed")
        }
    }, [open, winContext?.window])

    React.useEffect(() => {

        getAllProducts().then((result) => {
            setAllProducts(result.data)
        }).catch((err) => {
            alert("Unable to fetch Products")
        })

        socket.on("getBillingWindowStatus", () => {
            socket.emit("billingWindowStatus", (open || winContext?.window === 'billingWindow') ? true : false)
        })

        socket.on("openBillingWindow", () => {
            winContext?.setWindow("billingWindow")
        })

        socket.on("addProduct", (productID) => {
            getProductById(productID).then((prod) => {
                if (prod) {
                    if (isAvailable(prod.data) >= 0) {
                        addMoreQty(prod.data)
                    } else {
                        setPurchasedProducts((pre) => [...pre, { ...prod.data, buyQuantity: 1 }])
                    }
                }
            }).catch((err) => {
                socket.emit("errorWhileAddingProduct", err.response.data)
                console.log(err)
            })
        })
    }, [socket])

    // const handleAddMoreQuantity = (ind: number) => {
    //     let modified: Array<IPurchasedProducts> = []
    //     purchasedProducts.map((prod, i) => {
    //         if (i === ind)
    //             prod.buyQuantity! += 1
    //         return modified.push(prod)
    //     })
    //     setPurchasedProducts(modified)
    // }

    const handleRemoveQuantity = (ind: number) => {
        if (purchasedProducts[ind].buyQuantity! > 1) {
            let modified: Array<IPurchasedProducts> = []
            purchasedProducts.map((prod, i) => {
                if (i === ind)
                    prod.buyQuantity! -= 1
                return modified.push(prod)
            })
            setPurchasedProducts(modified)
        }

    }


    const addMoreQty = (prod: IPurchasedProducts) => {
        setPurchasedProducts(prods => [...prods, prod])
    }

    const removeQty = (prod: IPurchasedProducts) => {
        let removed = false;
        const newProds: IPurchasedProducts[] = []
        for (const product of purchasedProducts) {
            if (!removed && product._id === prod._id) {
                removed = true;
                continue;
            }
            newProds.push(product)
        }
        if (newProds.length === purchasedProducts.length - 1) {
            setPurchasedProducts(newProds)
        }
    }

    function valuetext(value: number) {
        return `${value}°C`;
    }




    // document.body.addEventListener('keydown',keyDown)

    return (
        <div>
            {/* <Button variant="outlined" >
                Open full-screen dialog
            </Button> */}
            <Fab color="success" aria-label="New Bill" onClick={handleClickOpen} sx={{ position: "fixed", bottom: { xs: "60px", md: "15px", lg: "15px" }, right: "20px" }}>
                <AddShoppingCartOutlinedIcon />
            </Fab>
            {
                allProducts &&

                <Dialog
                    fullScreen
                    open={open || winContext?.window === 'billingWindow'}
                    onClose={handleClose}
                    TransitionComponent={Transition}
                >
                    <AppBar position="sticky">
                        <Toolbar>
                            <IconButton
                                edge="start"
                                color="inherit"
                                onClick={handleClose}
                                aria-label="close"
                            >
                                <CloseIcon />
                            </IconButton>
                            <Typography sx={{ ml: 2, flex: 1 }} variant="h6" component="div">
                                New Bill
                            </Typography>
                            <Button autoFocus color="inherit" onClick={handleSaveAndPrint}>
                                save and Print
                            </Button>
                        </Toolbar>
                        <Divider />
                        <Box sx={{ display: "flex", flexDirection: "row", margin: "3px", maxWidth: "500px", marginLeft: { md: "30%" } }}>
                            <Typography sx={{ ml: 2, flex: 1, textAlign: "left" }} variant="subtitle2" component="span">
                                Net Amount
                            </Typography>
                            <Typography sx={{ mr: 2, flex: 1, textAlign: "right" }} variant="subtitle2" component="span">
                                Rs.{(billAmount - ((Number(discount) / 100) * billAmount)).toFixed(0)}/-
                            </Typography>
                        </Box>
                    </AppBar>
                    {
                        error && <PopupAlert type={error.type} msg={error.msg} setError={setError} />
                    }


                    <List sx={{ marginBottom: "80px" }}>
                        <Grid container sx={{ p: 2 }} spacing={{ xs: 1, sm: 2, md: 2 }} columns={{ xs: 2, sm: 6, md: 12 }}>
                            {
                                purchasedProductsConc && purchasedProductsConc.map((product: IPurchasedProducts, ind) =>
                                    <Grid item key={ind} xs={2} sm={3} md={4}>
                                        <Card elevation={1}>
                                            <ListItem button>
                                                <ListItemAvatar>
                                                    <Skeleton animation="wave" variant="circular" width={40} height={40} >
                                                        <Avatar />
                                                    </Skeleton>
                                                </ListItemAvatar>
                                                <Grid container columns={12}>
                                                    <Grid item xs={5} md={5} lg={5}>
                                                        <ListItemText primary={product.productName} secondary={`${product.productCategory} - ${product.productSubCategory}`} />
                                                    </Grid>
                                                    <Grid item xs={2} md={2} lg={2} sx={{ textAlign: "center", mt: 2 }}>
                                                        <Chip label={product.productSize} color="primary" variant="outlined" size="small" sx={{ marginBottom: "2px" }} />
                                                    </Grid>
                                                    <Grid item xs={3} md={3} lg={3} sx={{ textAlign: "center", mt: { md: 2, lg: 2 } }}>
                                                        <Typography variant="subtitle1" color="success" component={"p"}>
                                                            {`Rs. ${product.productPrice} x ${product.buyQuantity}`}
                                                        </Typography>
                                                    </Grid>
                                                    <Grid item xs={2} md={2} lg={2}>
                                                        <Button size="small" onClick={() => addMoreQty(product)}><AddIcon /></Button>
                                                        <Button size="small" onClick={() => removeQty(product)}><RemoveIcon /></Button>
                                                    </Grid>
                                                </Grid>
                                            </ListItem>
                                        </Card>
                                        {/* <Divider /> */}
                                    </Grid>
                                )
                            }

                            <Grid item xs={2} sm={3} md={4}>
                                <Card elevation={1}>
                                    <ListItem>
                                        <Grid container columns={12}>
                                            <Grid item xs={7} md={7} lg={7} sm={7}>
                                                {/* <ListItemText primary="Discount" secondary={`${discount}% on ${billAmount}`} /> */}
                                                <Box textAlign={"center"}>
                                                    <Typography>
                                                        Discount
                                                        <Typography variant='caption'>
                                                            {`  =>  ${discount}% on ${billAmount}`}
                                                        </Typography>
                                                    </Typography>
                                                </Box>
                                                <Slider aria-label="Discount" defaultValue={0} getAriaValueText={valuetext} valueLabelDisplay="off" step={2.5} marks min={0} max={30} sx={{ maxWidth: "70%", marginLeft: "15%" }}
                                                    onChange={(event: Event, value: number | number[], activeThumb: number) => {
                                                        setDiscount(value)
                                                    }}
                                                />
                                            </Grid>
                                            <Grid item xs={5} md={5} lg={5} sm={5}>
                                                <Typography textAlign={"center"} component={"div"}>
                                                    Payment Mode
                                                    <ToggleButtonGroup sx={{ height: "10px" }} color="primary" value={paymentMode} exclusive onChange={(e, value: any) => { setPaymentMode(value) }}>
                                                        <ToggleButton value="Online" sx={{ width: "70px" }}>Online</ToggleButton>
                                                        <ToggleButton value="Cash" sx={{ width: "70px" }}>Cash</ToggleButton>
                                                    </ToggleButtonGroup>
                                                </Typography>
                                            </Grid>
                                        </Grid>

                                    </ListItem>
                                </Card>
                            </Grid>
                        </Grid>
                    </List>
                    <SearchProduct setSelectedProducts={setPurchasedProducts} allProducts={allProducts} purchasedProducts={purchasedProducts} />
                </Dialog>
            }
            {
                printPreview && <PrintPreview bill={bill!} open={printPreview} onClose={handlePrintPreviewClose} />
            }
        </div>
    );
}
