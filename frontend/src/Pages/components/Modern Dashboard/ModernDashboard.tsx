import { Alert, AppBar, Backdrop, Box, Button, Card, CircularProgress, Divider, Grid, List, ListItem, ListItemButton, ListItemText, Paper, Snackbar, Tab, Tabs, Typography, useTheme, Zoom } from '@mui/material'
import React, { useContext, useEffect } from 'react'
import ReactApexChart from 'react-apexcharts'
import { ApexOptions } from 'apexcharts'
import { IBill } from '../interfaces/bill'
import PrintPreview from '../PrintPreview'
import Clock from 'react-live-clock';
import BillingWindow from '../BillingWindow'
import ReportGeneratorDialog from '../ReportGenerator'
import DailyExpense from '../DailyExpense'
import { getRecentNBills } from '../services/billing.service'
import { BillingReport } from '../billingReport'
import { socketContext, WindowContext } from '../../../App'
import { io } from 'socket.io-client'
// import SwipeableViews from 'react-swipeable-views';

const Linechart = (props: any) => {
    return (
        <ReactApexChart options={props.options} series={props.data} />
    )
}

const ProductsAndBills = ({ setOpenBill, setBill }: any) => {

    // const orders = Array.from({ length: 30 }, (_, i) => { return { orderId: (i + 1), orderNumber: "Order Number" + (i + 1), orderAmount: "Rs.500" } })

    const [orders, setOrders] = React.useState<IBill[]>();
    const [billingReportOpen, setBillingReportOpen] = React.useState(false);


    const theme = useTheme()
    const [value, setValue] = React.useState(0);


    const tabs = ["Products", "Bills"]

    const handleChange = (event: React.SyntheticEvent, newValue: number) => {
        setValue(newValue);
    };

    const handleChangeIndex = (index: number) => {
        setValue(index);
    };


    const handleListItemClick = (event: React.MouseEvent<HTMLDivElement, MouseEvent>, bill: IBill,) => {
        // let newBill: IBill = { billNumber: index, products: [{ productName: "ABC Product", inStock: 2, productCategory: "Men's Wear", productCode: "OHC", productPrice: 80, productSize: "34", productSubCategory: "Shirt", buyQuantity: 5 }], amount: 120, date: `${new Date().getDate()}/${new Date().getMonth() + 1}/${new Date().getFullYear()}`, time: `${new Date().toLocaleTimeString()}`, discount: 12, paymentMode: "Online" }
        setOpenBill(true)
        setBill(bill)
    };

    const viewAllBills = () => {
        setBillingReportOpen(true);
    };

    useEffect(() => {
        getRecentNBills(31).then((bills) => {
            if (bills) {
                setOrders(bills.data)
            }
        }).catch((err) => {
            console.log("Error while fetching Bills => ", err)
        })
    }, [])

    return (
        <>
            <Tabs
                value={value}
                onChange={handleChange}
                indicatorColor="secondary"
                textColor="inherit"
                variant="fullWidth"
                aria-label="full width tabs example">

                {
                    tabs.map((tab, ind) => <Tab label={tab} key={`Tab-${tab}-${ind}`} id={`full-width-tab-${tab}`} aria-controls={`full-width-tabpanel-${tab}`} />)
                }
            </Tabs>

            <Box sx={{ height: "230px", overflowY: "scroll" }}>
                <Card role="tabpanel" hidden={value !== 0} id="full-width-tabpanel-Products" aria-labelledby="full-width-tab-Products">
                    Product Chart
                </Card>

                <Card role="tabpanel" hidden={value !== 1} id="full-width-tabpanel-Monthly_Sale" aria-labelledby="full-width-tab-Monthly_Sale">
                    <List component="nav" aria-label="main mailbox folders" sx={{}} dense>
                        <Grid container spacing={0} columns={{ xs: 2, sm: 2, md: 4, lg: 4 }}>
                            {
                                orders && orders.map((order: any, ind: number) =>
                                    <Grid item md={1} xs={1} sm={1} lg={1} key={ind} zeroMinWidth={true}>
                                        <ListItemButton key={ind} onClick={(event: any) => handleListItemClick(event, order)}>
                                            <ListItemText primary={`Bill Number ${order.billNumber}`} secondary={`Amount : ${order.amount}`} />
                                        </ListItemButton>
                                    </Grid>
                                )
                            }
                            <Grid item md={1} xs={1} sm={1} lg={1} key={"ViewAllBills"} zeroMinWidth={true}>
                                <ListItemButton key={"View All"} sx={{ boxShadow: "1px black" }} onClick={(event: any) => viewAllBills()}>
                                    <ListItemText primary={`All Bills`} secondary={"..."} />
                                </ListItemButton>
                            </Grid>
                        </Grid>
                    </List>
                </Card>
            </Box>
            {
                <BillingReport setBillingReportOpen={setBillingReportOpen} billingReportOpen={billingReportOpen} handleListItemClick={handleListItemClick} />
            }
        </>
    )
}

const DailyExpenseAndPayments = ({ setOpenBill, setBill }: any) => {

    const orders = Array.from({ length: 30 }, (_, i) => { return { orderId: (i + 1), orderNumber: "Order Number" + (i + 1), orderAmount: "Rs.500" } })
    const theme = useTheme()
    const [value, setValue] = React.useState(0);


    const tabs = ["Daily Expense", "Payments"]

    const handleChange = (event: React.SyntheticEvent, newValue: number) => {
        setValue(newValue);
    };

    const handleChangeIndex = (index: number) => {
        setValue(index);
    };


    const handleListItemClick = (event: React.MouseEvent<HTMLDivElement, MouseEvent>, bill: IBill,) => {
        // let newBill: IBill = { billNumber: index, products: [{ productName: "ABC Product", inStock: 2, productCategory: "Men's Wear", productCode: "OHC", productPrice: 80, productSize: "34", productSubCategory: "Shirt", buyQuantity: 5 }], amount: 120, date: `${new Date().getDate()}/${new Date().getMonth() + 1}/${new Date().getFullYear()}`, time: `${new Date().toLocaleTimeString()}`, discount: 12, paymentMode: "Online" }
        setOpenBill(true)
        setBill(bill)
    };

    return (
        <>
            <Tabs
                value={value}
                onChange={handleChange}
                indicatorColor="secondary"
                textColor="inherit"
                variant="fullWidth"
                aria-label="full width tabs example">

                {
                    tabs.map((tab, ind) => <Tab label={tab} key={`Tab-${tab}-${ind}`} id={`full-width-tab-${tab}`} aria-controls={`full-width-tabpanel-${tab}`} />)
                }
            </Tabs>

            <Box sx={{ height: "230px", overflowY: "scroll" }}>
                <Card role="tabpanel" hidden={value !== 0} id="full-width-tabpanel-Products" aria-labelledby="full-width-tab-Products">
                    <DailyExpense />
                </Card>

                <Card role="tabpanel" hidden={value !== 1} id="full-width-tabpanel-Monthly_Sale" aria-labelledby="full-width-tab-Monthly_Sale">
                    <List component="nav" aria-label="main mailbox folders" sx={{}}>
                        <Grid container spacing={1} columns={{ xs: 2, sm: 2, md: 3, lg: 3 }}>
                            {
                                orders && orders.map((order: any, ind: number) =>
                                    <Grid item md={1} xs={1} sm={1} lg={1} key={ind}>
                                        <ListItemButton key={ind} onClick={(event: any) => handleListItemClick(event, order)}>
                                            <ListItemText primary={order.orderNumber} />
                                        </ListItemButton>
                                    </Grid>
                                )
                            }
                        </Grid>
                    </List>
                </Card>
            </Box>
        </>
    )
}


export const ModernDashboard = () => {

    const [openBill, setOpenBill] = React.useState(false);
    const [bill, setBill] = React.useState<IBill>();
    const [alert, setAlert] = React.useState<{ type: any, msg: string }>();

    const winContext = useContext(WindowContext);
    const socket = useContext(socketContext)

    const openProductWindow = () => {
        winContext?.setWindow("reportWindow")
    }

    useEffect(() => {
        socket.emit("join", { deviceType: "desktop" })

        socket.on("alert", (type, msg) => {
            setAlert({ msg: msg, type: type })
        })

        socket.io.engine.on("error", (err) => {
            setAlert({ msg: "Error in socket" + err.toString(), type: "error" })
        })

        // socket.on("connect_error", (error) => {
        //     // if (error.message === "xhr poll error") {
        //     //     window.location.replace("https://" + window.location.hostname + ":8081/")
        //     // }
        //     setAlert({ msg: error.message, type: "error" })
        // })

        socket.emit("isScannerOnline")

        socket.io.on("reconnect_attempt", (attempt) => {
            console.log("Reconnecting")
            setAlert({ msg: "Reconnecting..." + attempt.toString(), type: "warning" })
        })

        socket.io.on("reconnect", () => {
            socket.emit("join", { deviceType: "desktop" })
        })

        return () => { socket.disconnect() }
    }, [socket])

    const options: ApexOptions = {
        chart: {
            type: 'area',
            height: '100px',
            width: "150px",
            sparkline: {
                enabled: true
            },
            redrawOnWindowResize: true,
            redrawOnParentResize: true
        },
        stroke: {
            curve: 'straight'
        },
        fill: {
            opacity: 0.3,
        },
        yaxis: {
            min: 0
        },
        colors: ['#DCE6EC'],
        title: {
            text: '$424,652',
            offsetX: 0,
            style: {
                fontSize: '24px',
            }
        },
        subtitle: {
            text: 'Sales',
            offsetX: 0,
            style: {
                fontSize: '14px',
            }
        }
    }

    const series = [{
        data: [25, 66, 41, 89, 63, 25, 44, 12, 36, 9, 54, 25, 66, 41, 89, 63, 25, 44, 12, 36, 9, 54, 25, 66, 41, 89, 63, 25, 44, 12, 36, 9, 54]
    }]

    const chartStyle = { paddingLeft: 2, paddingRight: 2, paddingTop: 3, paddingBottom: 3 }

    return (
        <Grid>
            <Grid container columns={3}>
                <Grid item xs={1} sm={1} md={1} lg={1} display="flex" flexDirection={"column"} justifyContent="left" alignItems="left">
                    <Typography sx={{ color: "gray" }}>{` ${new Date().toDateString()} `}</Typography>
                    <Typography sx={{ color: "gray" }}><Clock format={'HH:mm:ss'} ticking={true} timezone={'Asia/Calcutta'} /></Typography>
                </Grid>
                <Grid item xs={1} sm={1} md={1} lg={1} display="flex" justifyContent="center" alignItems="center">
                    {alert &&
                        <Zoom in={alert !== undefined}>
                            <Alert severity={alert?.type} role="alert" >{alert?.msg}</Alert>
                        </Zoom>
                    }
                </Grid>
                <Grid item xs={1} sm={1} md={1} lg={1} display="flex" justifyContent="right" alignItems="right">
                    <Button variant='outlined' onClick={() => openProductWindow()}>Reports</Button>
                </Grid>
            </Grid>
            <Grid container columns={3} >
                <Grid item xs={1} sm={1} md={1} lg={1} sx={chartStyle}>
                    <Card sx={{ boxShadow: "0px 1px 22px -12px #607D8B" }}>
                        <Linechart options={options} data={series} />
                    </Card>
                </Grid>
                <Grid item xs={1} sm={1} md={1} lg={1} sx={chartStyle}>
                    <Card sx={{ boxShadow: "0px 1px 22px -12px #607D8B" }}>
                        <Linechart options={options} data={series} />
                    </Card>
                </Grid>
                <Grid item xs={1} sm={1} md={1} lg={1} sx={chartStyle}>
                    <Card sx={{ boxShadow: "0px 1px 22px -12px #607D8B" }}>
                        <Linechart options={options} data={series} />
                    </Card>
                </Grid>
            </Grid>
            <Grid container columns={2}>
                <Grid item xs={1} sm={1} md={1} lg={1} sx={chartStyle}>
                    <Card sx={{ boxShadow: "0px 1px 22px -12px #607D8B" }}>
                        <ProductsAndBills setOpenBill={setOpenBill} setBill={setBill} />
                    </Card>
                </Grid>
                <Grid item xs={1} sm={1} md={1} lg={1} sx={chartStyle}>
                    <Card sx={{ boxShadow: "0px 1px 22px -12px #607D8B" }}>
                        <DailyExpenseAndPayments setOpenBill={setOpenBill} setBill={setBill} />
                    </Card>
                </Grid>
            </Grid>
            <BillingWindow />
            {
                openBill && <PrintPreview bill={bill!} open={openBill} onClose={() => setOpenBill(false)} />
            }
        </Grid>
    )
}
