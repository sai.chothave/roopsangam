import * as React from 'react';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import { Autocomplete, Box, FormControl, InputLabel, MenuItem, Select, SelectChangeEvent, createFilterOptions, Grid, Alert, AlertColor, DialogContentText } from '@mui/material';
import { IProduct } from './interfaces/product';
import { addProduct, getValuesByFieldName } from './services/product.service';
import { WindowContext } from '../../App';


export function AutocompleteField(props: any) {
  const { value, setValue, label, fieldName } = props
  const [options, setOptions] = React.useState<any>([]);
  const filter = createFilterOptions<string>();

  React.useEffect(() => {
    getValuesByFieldName(fieldName).then((result) => {
      setOptions(result.data)
    }).catch((err) => {
      console.log(err.response)
      alert("something went wrong")
    })
  }, [])

  return (
    <>{
      options && <Autocomplete
        value={value}
        onChange={(event, newValue) => {
          if (typeof newValue === 'string') {
            setValue(newValue);
          } else if (newValue && newValue.inputValue) {
            setValue(newValue.inputValue);
          } else {
            setValue(newValue);
          }
        }}

        filterOptions={(options, params) => {
          const filtered = filter(options, params);
          const { inputValue } = params;
          const isExisting = options.some((option) => inputValue === option);
          if (inputValue !== '' && !isExisting) {
            filtered.push(inputValue);
          }
          return filtered;
        }}

        selectOnFocus
        clearOnBlur
        handleHomeEndKeys
        id={`id-${label}`}
        options={options!}
        freeSolo
        renderOption={(props, option) => <li {...props}>{option}</li>}
        renderInput={(params) => (
          <TextField {...params} label={label} variant="standard" />
        )}
      />
    }</>
  )

}

export default function AddProduct(props: any) {
  const { open, setOpen } = props

  const [productName, setProductName] = React.useState<string>("");
  const [productCategory, setProductCategory] = React.useState<string>("");
  const [productSubCategory, setProductSubCategory] = React.useState<string>("");
  const [productPrice, setProductPrice] = React.useState<number>(0);
  const [inStock, setInStock] = React.useState<number>(0);
  const [productCode, setProductCode] = React.useState<string>("");
  const [productSize, setProductSize] = React.useState<string>("");
  const winContext = React.useContext(WindowContext)

  const [alert, setAlert] = React.useState<{ type: AlertColor, msg: string }>();

  const handleSubmit = () => {
    const product: IProduct = {
      productName: productName!.toLocaleUpperCase(),
      productCategory: productCategory!.toLocaleUpperCase(),
      productCode: productCode!.toLocaleUpperCase(),
      productPrice: productPrice!,
      productSize: productSize!,
      productSubCategory: productSubCategory!.toLocaleUpperCase(),
      inStock: inStock!
    }
    addProduct(product).then((result) => {
      setAlert({ msg: "Product [ " + result.data.productName + "-" + result.data.productSize + " ] Added", type: "success" })
      setTimeout(()=>{
        handleClose()
      },1000)
    }).catch((err) => {
      setAlert({ msg: err.response.data, type: "error" })
    })
  }

  const handleChange = (event: SelectChangeEvent) => {
    setProductCategory(event.target.value);
  };

  const handleClose = () => {
    setOpen(false);
    winContext?.setWindow(false)
  };

  return (
    <div>
      <Dialog open={open || winContext?.window==="newProductWindow"} onClose={handleClose}>
        <DialogTitle>Add New Product</DialogTitle>
        <DialogContent>
          {
            alert && <Alert severity={alert.type}>{alert.msg}</Alert>
          }
          <Box component="form" sx={{ '& .MuiTextField-root': { m: 1 }, }} noValidate autoComplete="off">
            <Grid container columns={2} spacing={2}>
              <Grid item xs={2} sm={2} md={2} lg={2}>
                {/* <TextField id="input-with-sx" name="productName" fullWidth defaultValue={productName} label="Product Name" variant="standard" onChange={(e) => setProductName(e.target.value)} /> */}
                <AutocompleteField value={productName} setValue={setProductName} fieldName={"productName"} label="Product Name" />
              </Grid>

              <Grid item xs={1} sm={1} md={1} lg={1}>
                <FormControl required sx={{ m: 1 }} fullWidth>
                  <InputLabel id="productCategory-label">Product Category</InputLabel>
                  <Select label="Product Category" variant="standard" value={productCategory} onChange={handleChange}>
                    <MenuItem value=""><em>None</em></MenuItem>
                    <MenuItem value="mens wear">Men's Wear</MenuItem>
                    <MenuItem value="womens wear">Women's Wear</MenuItem>
                  </Select>
                </FormControl>
              </Grid>
              <Grid item xs={1} sm={1} md={1} lg={1}>
                <AutocompleteField value={productSubCategory} setValue={setProductSubCategory} fieldName={"productSubCategory"} label="Product SubCategory" />
              </Grid>

              <Grid item xs={1} sm={1} md={1} lg={1}>
                <TextField id="productPrice" name="productPrice" fullWidth value={productPrice} label="Product Price" variant="standard" onChange={(e) => setProductPrice(Number(e.target.value))} />
              </Grid>
              <Grid item xs={1} sm={1} md={1} lg={1}>
                <AutocompleteField value={productCode} setValue={setProductCode} fieldName={"productCode"} label="product Code" />
              </Grid>

              <Grid item xs={1} sm={1} md={1} lg={1}>
                <TextField id="inStock" name="inStock" fullWidth value={inStock} label="Available Stock" variant="standard" onChange={(e) => setInStock(Number(e.target.value))} />
              </Grid>
              <Grid item xs={1} sm={1} md={1} lg={1}>
                <AutocompleteField value={productSize} setValue={setProductSize} fieldName={"productSize"} label="Product Size" />
              </Grid>
            </Grid>
          </Box>
          <DialogContentText>
            * To select Press Enter on click from dropdown values.
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Cancel</Button>
          <Button onClick={handleSubmit}>Add</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
