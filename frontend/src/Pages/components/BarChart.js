import React, { useEffect, useState } from "react";
import ReactApexChart from "react-apexcharts";

export const BarChart = (props) => {
    const [chartData, initChart] = useState();

    useEffect(() => {
        initChart(
            {
                options: {
                    plotOptions: {
                        bar: {
                            dataLabels: {
                                position: "top" // top, center, bottom
                            }
                        }
                    },
                    dataLabels: {
                        enabled: false,
                        formatter: function (val) {
                            return "Rs." + Number(val).toLocaleString();
                        },
                        offsetY: -20,
                        style: {
                            fontSize: "12px",
                            colors: ["#304758"]
                        }
                    },
                    xaxis: {
                        categories: props.labels,
                        position: "bottom",
                        labels: {
                            offsetY: 0,
                            show:false
                        },
                        axisBorder: {
                            show: false
                        },
                        axisTicks: {
                            show: false
                        },
                        // labels: {
                        //     show: false
                        // },
                        crosshairs_: {
                            fill: {
                                type: "gradient",
                                gradient: {
                                    colorFrom: "#D8E3F0",
                                    colorTo: "#BED1E6",
                                    stops: [0, 100],
                                    opacityFrom: 0.4,
                                    opacityTo: 0.5
                                }
                            }
                        },
                        tooltip: {
                            enabled: false,
                            offsetY: -35
                        }
                    },
                    fill: {
                        gradient: {
                            shade: "light",
                            type: "horizontal",
                            shadeIntensity: 0.25,
                            gradientToColors: undefined,
                            inverseColors: true,
                            opacityFrom: 1,
                            opacityTo: 1,
                            stops: [50, 0, 100, 100]
                        }
                    },
                    yaxis: {
                        axisBorder: {
                            show: true
                        },
                        axisTicks: {
                            show: true
                        },
                        labels: {
                            show: false,
                            formatter: function (val) {
                                return "Rs." + Number(val).toLocaleString();
                            }
                        }
                    },
                    title: {
                        text: props.heading,
                        floating: true,
                        offsetY: 0,
                        align: "center",
                        style: {
                            color: "#444"
                        }
                    },
                    chart: {
                        animations: {
                            enabled: true
                        },
                    }
                },
                series: [
                    {
                        name: props.month,
                        data: props.data
                    }
                ]
            }
        )
    }, [props])


    return (
        <div id="chart">
            {
                chartData && <ReactApexChart
                    options={chartData.options}
                    series={chartData.series}
                    type="line"
                    height={props.height}
                    width={"100%"}
                />
            }
        </div>
    );
}
