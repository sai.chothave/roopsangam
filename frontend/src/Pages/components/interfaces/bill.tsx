import { IProduct, IPurchasedProducts } from "./product";

export  interface IBill {
    billNumber : number,
    products : IPurchasedProducts[],
    paymentMode : "Online" | "Cash",
    date : string,
    time : string,
    amount : number,
    discount : number | number[]
}