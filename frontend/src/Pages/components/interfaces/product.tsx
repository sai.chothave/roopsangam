export  interface IProduct {
    _id?:string,
    productName: string,
    productCategory: string,
    productSubCategory: string,
    productPrice: number,
    inStock: number,
    productCode:string,
    productSize:string
}

export  interface IProducts {
    products: Array<IProduct>,
    filters: Array<string>
}

export interface IProductDB extends IProduct {
    _id:string
}


export interface IPurchasedProducts extends IProduct {
    productID?:any
    buyQuantity? : 0 | number
}


