import * as React from 'react';
import Box from '@mui/material/Box';
import { DataGrid, GridToolbar, GridCallbackDetails, GridColDef, MuiBaseEvent, MuiEvent, GridRenderCellParams, GridValueGetterParams } from '@mui/x-data-grid';
// import { DataGrid,  } from '@mui/x-data-grid';
import ReceiptLongIcon from '@mui/icons-material/ReceiptLong';
import RemoveShoppingCartIcon from '@mui/icons-material/RemoveShoppingCart';

import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import CloseIcon from '@mui/icons-material/Close';
import IconButton from '@mui/material/IconButton';
import { Alert, AlertColor, Collapse, DialogContentText, Grid, Table, TableBody, TableCell, TableHead, TableRow, Typography } from '@mui/material';
import AddProduct from './AddProduct';
import { IBill } from './interfaces/bill';
import { getAllBills, updateBillStatus } from './services/billing.service';
import { WindowContext } from '../../App';
import PrintPreview from './PrintPreview';
import ConfirmationAlert from './ConfirmationAlert';


export const BillingReportTable = (props: any) => {
  const [bills, setBills] = React.useState<Array<IBill>>()
  const [pageSize, setPageSize] = React.useState<number>(10);
  const [openBill, setOpenBill] = React.useState(false);
  const [bill, setBill] = React.useState<IBill>()
  const [openConfirmation, setOpenConfirmation] = React.useState(false);
  const [refreshTable,setRefreshTable] = React.useState(0)
  // const [error, setError] = React.useState<{ msg: string, type: AlertColor }>()

  const openBillPreview = (billToOpen: IBill) => {
    setBill(billToOpen)
    setOpenBill(true)
  }

  const showAcceptReturn = (billToReturn: IBill) => {
    setBill(billToReturn)
    setOpenConfirmation(true)
  }

  const acceptReturn = () => {
    updateBillStatus(bill).then((response) => {
      props.setAlert({ msg: response.data, type: "success" })
      setOpenConfirmation(false)
      setRefreshTable(refreshTable+1)
    }).catch((error) => {
      props.setAlert({ msg: "Unable to process => " + error.response.data, type: "error" })
      setOpenConfirmation(false)
      setRefreshTable(refreshTable+1)
      alert("Unable to accept Return :: " + error.response.message)
    })
    setTimeout(()=>{
      props.setAlert(undefined)
    },3000)
    
  }

  const columns: GridColDef[] = [
    { field: 'id', headerName: 'ID', width: 90, disableColumnMenu: true, hide: true },
    {
      field: 'billNumber',
      headerName: 'Bill Number',
      width: 150,
      editable: false,
    },
    {
      field: 'date',
      headerName: 'Date',
      width: 110,
      editable: false,
      renderCell: (params: GridRenderCellParams<any, any, any>) => `${new Date(params.row.date).getDate()}/${new Date(params.row.date).getMonth() + 1}/${new Date(params.row.date).getFullYear()}`
    },
    {
      field: 'time',
      headerName: 'Time',
      width: 110,
      editable: false,
    },
    {
      field: 'amount',
      headerName: 'Amount',
      width: 120,
      editable: false,
      renderCell: (params: GridRenderCellParams<any, any, any>) => "Rs." + params.row.amount + '/-'
    },
    {
      field: 'discount',
      headerName: 'Discount',
      width: 120,
      editable: false,
      renderCell: (params: GridRenderCellParams<any, any, any>) => params.row.discount + "%"
    }, {
      field: 'paymentMode',
      headerName: 'Payment Mode',
      width: 120,
      editable: false,
      // renderCell: (params: GridRenderCellParams<any, any, any>) => "Rs."+params.row.amount+'/-'
    }, {
      field: 'status',
      headerName: 'Status',
      width: 150,
      editable: false,
      // renderCell: (params: GridRenderCellParams<any, any, any>) => "Rs."+params.row.amount+'/-'
    }, {
      field: 'action',
      headerName: 'Action',
      width: 120,
      editable: false,
      renderCell: (params: GridRenderCellParams<any, any, any>) => <>
        <IconButton size='small' title={"View [ Bill Number " + params.row.billNumber + " ]"} onClick={(event: any) => { openBillPreview(params.row) }}>
          <ReceiptLongIcon />
        </IconButton>
        <IconButton size='small' disabled={params.row.status === "Return Accepted"} title={"Return [ Bill Number " + params.row.billNumber + " ]"} onClick={(event: any) => { showAcceptReturn(params.row) }}>
          <RemoveShoppingCartIcon />
        </IconButton>
      </>
    },

  ]

  React.useEffect(() => {
    getAllBills().then((result) => {
      let nprod: IBill[] = []
      result.data.map((prod: any, ind: number) => {
        return nprod.push({ id: prod._id, ...prod })
      })
      console.log("Rows", nprod)
      setBills(nprod)
    }).catch((err) => {
      alert("Unable to load bills")
    })
  }, [refreshTable])

  React.useEffect(()=>{
    props.setAlert(undefined)
  },[])

  const getAlertBody = () => {
    return (
      <Box>
        <Alert variant="standard" severity='warning'>Are you sure to accept returned products?</Alert>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Bill Numbar</TableCell>
              <TableCell>Date</TableCell>
              <TableCell>Discount</TableCell>
              <TableCell>Amount</TableCell>
              <TableCell>Payment Mode</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableCell>{bill?.billNumber}</TableCell>
            <TableCell>{bill?.date.split("T")[0]}</TableCell>
            <TableCell>{bill?.discount+"%"}</TableCell>
            <TableCell>{"Rs."+bill?.amount+"/-"}</TableCell>
            <TableCell>{bill?.paymentMode}</TableCell>
          </TableBody>
        </Table>
      </Box>
    )
  }

  return (
    <Box sx={{ display: "flex", height: 400, width: '100%' }}>
      {
        <DataGrid
          rows={(bills === undefined) ? [] : bills}
          columns={columns}
          pageSize={pageSize}
          onPageSizeChange={(newPageSize) => setPageSize(newPageSize)}
          rowsPerPageOptions={[10, 15, 20, 25, 30, 35]}
          pagination
          disableSelectionOnClick
          // onCellEditStop={updateValue}
          components={{ Toolbar: GridToolbar }}
          componentsProps={{
            toolbar: { showQuickFilter: true },
          }}
          loading={(bills === undefined) ? true : false}
          disableExtendRowFullWidth={false}
          sx={{ width: "100%" }}
        />
      }
      {
        openBill && <PrintPreview bill={bill!} open={openBill} onClose={() => setOpenBill(false)} />
      }{
        openConfirmation && <ConfirmationAlert open={openConfirmation} setOpen={setOpenConfirmation} body={getAlertBody()} title={"Confirmation...!"} callBack={acceptReturn} />
      }
    </Box>
  );

}



export const BillingReport = (props: any) => {
  const [alert, setAlert] = React.useState<{ type: any, msg: string }>();
  const [openAddProduct, setOpenAddProduct] = React.useState(false);

  return (
    <div>
      <Dialog
        fullWidth={true}
        maxWidth={'lg'}
        open={(props.open) ? true : false}
        onClose={() => props.close()}
        aria-labelledby="My bills"
      >
        <DialogTitle id="Mybills">
          <Grid container columns={2}>
            <Grid item sm={1} md={1} xs={1} lg={1}>
              {"My bills"}
            </Grid>
            <Grid item sm={1} md={1} xs={1} lg={1} textAlign="right">
              <Button variant='outlined' onClick={() => setOpenAddProduct(true)}>Add New</Button>
            </Grid>
          </Grid>
        </DialogTitle>
        <Collapse in={(alert === undefined) ? false : true}>
          {
            alert && <Alert
              severity={alert.type}
              action={
                <IconButton
                  aria-label="close"
                  color="inherit"
                  size="small"
                  onClick={() => {
                    setAlert(undefined);
                  }}
                >
                  <CloseIcon fontSize="inherit" />
                </IconButton>
              }
              sx={{ mb: 2 }}
            >
              {alert.msg}
            </Alert>
          }
        </Collapse>

        {/* {
                    alert && <Alert severity={alert.type}>{alert.msg}</Alert>
                } */}
        <DialogContent>
          <BillingReportTable openAddProduct={openAddProduct} setAlert={setAlert} handleListItemClick={props.handleListItemClick} />
          <DialogContentText>
            *Double click on cell to update value
          </DialogContentText>
        </DialogContent>
      </Dialog>

      {
        openAddProduct && <AddProduct open={openAddProduct} setOpen={setOpenAddProduct} alert={alert} />
      }
    </div>
  );
}
