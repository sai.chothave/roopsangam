import { Alert, Button, Card, Chip, Divider, Fab, Grid, Zoom } from '@mui/material';
import React, { useCallback, useContext, useEffect } from 'react'
import { io } from 'socket.io-client';
import { socketContext } from '../../../App';
import PopupAlert from '../PopupAlert';
import { Scanner } from './Scanner';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import { Camera, QrCode, QrCode2, ScannerSharp } from '@mui/icons-material';
import { OnResultFunction } from 'react-qr-reader';
// import { ObjectId } from 'mongodb';
import successSound from "./../audio/beep.mp3"
import failureSound from "./../audio/negativeBeep.mp3"
import { IPurchasedProducts } from '../interfaces/product';
import { groupBy } from 'lodash';
import { ShowPaymentQR } from '../ShowPaymentQR';
import { IBill } from '../interfaces/bill';

const defAlert = (msg: string) => {
  alert(msg);
}



const ButtonAppBar = () => {
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            Roopsangam
            <Typography variant='caption'>
              &nbsp;&nbsp;Scanner
            </Typography>
          </Typography>
          <Button color="inherit">Login</Button>
        </Toolbar>
      </AppBar>
    </Box>
  );
}

export const MobileScanner = () => {
  const [alert, setAlert] = React.useState<{ type: any, msg: string }>();
  const [openScanner, setOpenScanner] = React.useState(true);
  const [billingWindow, setBillingWindow] = React.useState(false);
  const [popUpAlert, setPopUpAlert] = React.useState<{ type: any, msg: string }>();
  const [purchasedProducts, setPurchasedProducts] = React.useState<Array<IPurchasedProducts>>([]);
  const [billAmount, setBillAmount] = React.useState<number>(0);
  const [openQR, setOpenQR] = React.useState(false);
  const [bill, setBill] = React.useState<IBill>();

  const socket = useContext(socketContext)

  const openBillingWindow = () => {
    socket.emit("openBillingWindow")
    setBillingWindow(true)
    // socket.emit("getBillingWindowStatus").on("billingWindowStatus",(status)=>{
    //   console.log(status)
    //   if(!status){

    //   }else{
    //     setAlert({msg:"Already Opened",type:"info"})
    //   }
    // })
  }

  useEffect(() => {
    setBillAmount(purchasedProducts?.reduce((total, prod) => { return total + (prod.productPrice * prod.buyQuantity!) }, 0))
  }, [purchasedProducts])


  useEffect(() => {

    socket.emit("join", { deviceType: "scanner" })

    socket.on("connect", () => {
      setAlert({ msg: "Connected", type: "success" })
    })

    socket.on("alert", (type, msg) => {
      setAlert({ msg: msg, type: type })
    })

    socket.io.engine.on("error", (err) => {
      setAlert({ msg: "Error in socket" + err.toString(), type: "error" })
    })

    // socket.on("connect_error", (error) => {
    //   // if (error.message === "xhr poll error") {
    //   //   window.location.replace("https://" + window.location.hostname + ":8081/")
    //   // }
    //   setAlert({ msg: error.message, type: "error" })
    // })

    socket.emit("isDesktopOnline")

    socket.io.on("reconnect_attempt", (attempt) => {
      setAlert({ msg: "Reconnecting..." + attempt.toString(), type: "warning" })
    })

    socket.io.on("reconnect", () => {
      socket.emit("join", { deviceType: "scanner" })
    })

    socket.on("billingWindowClosed", () => {
      setPopUpAlert({ msg: "Billing Window Closed", type: "info" })
      setBillingWindow(false)
      setPurchasedProducts([])
    })

    socket.on("billingWindowOpened", () => {
      setPopUpAlert({ msg: "Billing Window Opened", type: "info" })
      setBillingWindow(true)
    })

    socket.on("productAdded", (products) => {
      new Audio(successSound).play()
      setPopUpAlert({ msg: "Product Added", type: "success" })
      const res = groupBy(products, (prod: IPurchasedProducts) => prod._id)
      let purProd: any = [];
      for (const id in res) {
        purProd.push({ ...res[id][0], buyQuantity: res[id].length })
      }
      setPurchasedProducts(purProd)
    })

    socket.on("errorWhileAddingProduct",(err)=>{
      new Audio(failureSound).play()
      setPopUpAlert({ msg: err, type: "error" })
    })

    socket.on("acceptOnlinePayment",(bill)=>{
      setBill(bill)
      setOpenQR(true);
    })

    return () => { socket.disconnect() }
  }, [socket])

  const scanResult: OnResultFunction = useCallback((result, err) => {
    console.log(result)
    if (result !== null && result !== undefined) {
      try {
        if (result.getText().indexOf("RP") >= 0) {
          setOpenScanner(false)
          socket.emit("addProduct", result.getText().split("RP-")[1])
        }
      } catch (e) {
        setPopUpAlert({ msg: result.toString(), type: "error" })
      }
    }
  }, [])

  const getScannerDisableLog = () => {
    return (
      <Typography variant='caption' textAlign={"center"} sx={{ position: "relative", top: "50%" }}>
        {(alert?.type !== "success") ? alert?.msg : (!billingWindow) ? "Billing Window is closed" : (!openScanner) && "Click button to open Scanner"}
      </Typography>)
  }

  return (
    <>
      <ButtonAppBar />
      <Grid container direction={"column"} spacing={1} component={Card} justifyContent={"center"} alignItems={"center"} sx={{ backgroundColor: "#eff4f7" }}>
        <Grid item>
          {
            alert && <Zoom in={alert !== undefined}><Alert severity={alert?.type} variant={"standard"}>{alert.msg}</Alert></Zoom>
          }
        </Grid>
        <Grid item>
          <Card sx={{ height: "293px", width: "293px", textAlign: "center" }}>
            {
              (openScanner && (alert?.type === "success") && billingWindow === true) ? <Scanner scanResult={scanResult} /> : getScannerDisableLog()
            }
          </Card>
        </Grid>
        <Grid item component={Card} mt={2} m={1} sx={{ width: "80vw" }} p={2} textAlign={"center"}>
          <Grid key={"Header"} container columns={12} fontSize={"10pt"} textAlign={"center"} className="TR-row" >
            <Grid item md={1} sm={1} xs={1} lg={1}> Sr.No </Grid>
            <Grid item md={6} sm={6} xs={6} lg={6}><Typography fontSize={"inherit"} noWrap>Product</Typography></Grid>
            <Grid item md={2} sm={2} xs={2} lg={2}> Qty </Grid>
            <Grid item md={3} sm={3} xs={3} lg={3}> Price </Grid>
          </Grid>
          <Divider sx={{ mb: 1 }} />
          <div style={{maxHeight:"200px",overflowY:'scroll'}}>
          {
            purchasedProducts && purchasedProducts.map((prod: IPurchasedProducts, ind) =>
              <Grid key={ind} container columns={12} fontSize={"10pt"} textAlign={"center"} className="TR-row" >
                <Grid item md={1} sm={1} xs={1} lg={1}> {ind + 1} </Grid>
                <Grid item md={6} sm={6} xs={6} lg={6}><Typography fontSize={"inherit"} noWrap>{prod.productName} - {prod.productSize}</Typography></Grid>
                <Grid item md={2} sm={2} xs={2} lg={2}> {prod.buyQuantity} </Grid>
                <Grid item md={3} sm={3} xs={3} lg={3}> {prod.productPrice * prod.buyQuantity!} </Grid>
              </Grid>
            )
          }
          </div>
          <Divider sx={{ mt: 1,mb:1 }} />
          <Grid key={"Total"} container columns={12} fontSize={"10pt"} fontWeight={"bolder"}  className="TR-row" >
            <Grid item md={7} sm={7} xs={7} lg={7} textAlign={"left"}>Total Amount</Grid>
            {/* <Grid item md={2} sm={2} xs={2} lg={2}> - </Grid> */}
            <Grid item md={5} sm={5} xs={5} lg={5} textAlign={"right"}> {`Rs. ${billAmount}/-`} </Grid>
          </Grid>

        </Grid>

        {
          (billingWindow) ?
            <Fab color={alert?.type} variant="extended" size="medium" sx={{ position: "fixed", bottom: "100px" }} onClick={() => (alert?.type === "success") ? setOpenScanner((cam) => !cam) : defAlert("Scanner Not Connected")}>
              <QrCode />
              Add Product
            </Fab> :
            <Fab color={alert?.type} disabled={billingWindow} variant="extended" size="medium" sx={{ position: "fixed", bottom: "100px" }} onClick={() => (alert?.type === "success") ? openBillingWindow() : defAlert("Scanner Not Connected")}>
              <QrCode />
              New Bill
            </Fab>
        }
      </Grid>
      {
        popUpAlert && <PopupAlert type={popUpAlert.type} msg={popUpAlert.msg} setError={setPopUpAlert} />
      }
      {
        bill && <ShowPaymentQR open={openQR} onClose={()=>setOpenQR(false)} bill={bill}/>
      }
    </>
  )
}
