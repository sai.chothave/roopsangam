import { Card, Fade, Grid, Zoom } from '@mui/material'
import React from 'react'
import { OnResultFunction, QrReader, QrReaderProps } from 'react-qr-reader'
import "./scanner.scss"

export const Scanner = (props:any) => {
    const [result, setResult] = React.useState<any>()
    const [error, setError] = React.useState<any>()

    return (
        <>
            <Grid item p={2}>
                <Fade in={true}>
                    <div className='scan'>
                        <QrReader scanDelay={500} onResult={props.scanResult} constraints={{
                            facingMode: 'environment'
                        }} containerStyle={{ width: 250 }} />
                    </div>
                </Fade>
            </Grid>
        </>
    )
}
