import * as React from 'react';
import Box from '@mui/material/Box';
import { DataGrid, GridToolbar, GridCallbackDetails, GridColDef, MuiBaseEvent, MuiEvent, GridRenderCellParams, GridValueGetterParams } from '@mui/x-data-grid';
// import { DataGrid,  } from '@mui/x-data-grid';

import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import { getProducts } from './dataFetcher';
import { IProduct, IProductDB } from './interfaces/product';
import CloseIcon from '@mui/icons-material/Close';
import IconButton from '@mui/material/IconButton';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';

import { GridCellParams } from '@mui/x-data-grid';
import { Alert, Collapse, DialogContentText, Grid } from '@mui/material';
import AddProduct from './AddProduct';
import { deleteProduct, getAllProducts, updateProduct } from './services/product.service';
import { WindowContext } from '../../App';

export interface IProductRow extends IProduct {
    id: string
}

export function ReportGenerator(props: any) {
    const [products, setProducts] = React.useState<Array<IProductRow>>()
    const [pageSize, setPageSize] = React.useState<number>(10);
    const [refreshTable, setRefreshTable] = React.useState(0);

    const removeProduct = (id: string) => {
        deleteProduct(id).then((result) => {
            props.setAlert({ type: "success", msg: result.data })
        }).catch((err) => {
            props.setAlert({ type: "error", msg: `Error while deleting - [${err.response.data}]` })
        })

        setTimeout(() => {
            props.setAlert(undefined)
        }, 6000)
        setRefreshTable(refreshTable + 1)
    }

    const columns: GridColDef[] = [
        { field: 'id', headerName: 'ID', width: 90, disableColumnMenu: true, hide: true },
        {
            field: 'productName',
            headerName: 'Product Name',
            width: 250,
            editable: true,
        },
        {
            field: 'productCategory',
            headerName: 'Product Category',
            width: 200,
            editable: false,
        },
        {
            field: 'productSubCategory',
            headerName: 'Product SubCategory',
            width: 180,
            editable: true,
        },
        // {
        //     field: 'inStock',
        //     headerName: 'inStock',
        //     description: 'This column has a value getter and is not sortable.',
        //     sortable: false,
        //     width: 160,
        //     valueGetter: (params: GridValueGetterParams) =>
        //         `${params.row.firstName || ''} ${params.row.lastName || ''}`,
        // },
        {
            field: 'productSize',
            headerName: 'Product Size',
            width: 110,
            editable: true,
        },
        {
            field: 'inStock',
            headerName: 'inStock',
            type: 'number',
            width: 80,
            editable: true,
            align: "center",
            renderCell: (params: GridRenderCellParams<any, any, any>) => (params.row.inStock === 0) ?
                <strong style={{ color: "red" }}>
                    {params.value}
                </strong> : <p>{params.value}</p>,

        },
        {
            field: 'productCode',
            headerName: 'Product Code',
            width: 110,
            editable: true,
        },
        {
            field: 'productPrice',
            headerName: 'Product Price',
            type: 'number',
            width: 100,
            editable: true,
            align: "center"
        }, {
            field: 'action',
            headerName: "Actions",
            editable: false,
            disableExport: true,
            disableReorder: true,
            sortable: false,
            renderCell: (params: GridRenderCellParams<any, any, any>) =>
                <IconButton size='small' title={"Remove " + params.row.productName} onClick={() => { removeProduct(String(params.id)) }}>
                    <DeleteForeverIcon />
                </IconButton>

        },
    ];



    React.useEffect(() => {

        // getProductByCategory(props.category)?.then((prod) => {
        //     console.log(prod)
        //     setProducts(prod.data)
        //     setFilteredProducts(prod.data)
        // })

        getAllProducts().then((result) => {

            let nprod: IProductRow[] = []
            result.data.map((prod: any, ind: number) => {
                return nprod.push({ id: prod._id, ...prod })
            })
            setProducts(nprod)
        })

    }, [refreshTable, props.openAddProduct])

    const updateValue = (params: GridCellParams, event: MuiEvent<MuiBaseEvent>, details: GridCallbackDetails<any>) => {
        // console.log(params,details)
        // console.log(`Column Updated [${params.field}] => Old value : ${params.row[params.field]}, New Value => ${params.getValue(params.row.id, params.field)}`)
        if (params.getValue(params.row.id, params.field).trim() !== "") {
            const data = {
                [params.field]: `${params.getValue(params.row.id, params.field).trim().toLocaleUpperCase()}`
            }
            updateProduct(String(params.id), data).then((result) => {
                if (result) {
                    props.setAlert({ type: "success", msg: `Column Updated [${params.field}] => Old value : ${params.row[params.field]}, New Value => ${params.getValue(params.row.id, params.field)}` })
                    setRefreshTable(refreshTable + 1)
                } else {
                    props.setAlert({ type: "error", msg: `Error while updating [${params.field}]` })
                    console.log("Problem while updating Data => ", result)
                    setRefreshTable(refreshTable + 1)
                }
            }).catch((err) => {
                props.setAlert({ type: "error", msg: `Error while updating [${err.response.data}]` })
                console.log(err)
                setRefreshTable(refreshTable + 1)
            })
        } else {
            props.setAlert({ type: "error", msg: `Enter Valid Data :: [${params.field}]` })
            setRefreshTable(refreshTable + 1)
        }

        setTimeout(() => {
            props.setAlert(undefined)
        }, 6000)
        // setRefreshTable(refreshTable+1)
    }

    const updateCellValue = (row:any) => {
        if(row.value !== ""){
            let data:any=null;
            try{
                
                data = {[row.field]:row.value.trim().toLocaleUpperCase()}
            }catch(e){
                data = {[row.field]:Number(row.value)}
            } 
            updateProduct(String(row.id), data).then((result) => {
                if (result.data.acknowledged) {
                    props.setAlert({ type: "success", msg: `Column Updated [${row.field}] New Value => ${data[row.field]}` })
                    setRefreshTable(refreshTable + 1)
                } else {
                    props.setAlert({ type: "error", msg: `Error while updating [${row.field}]` })
                    console.log("Problem while updating Data => ", result)
                    setRefreshTable(refreshTable + 1)
                }
            }).catch((err) => {
                props.setAlert({ type: "error", msg: `Error while updating [${err.response.data}]` })
                console.log(err)
                setRefreshTable(refreshTable + 1)
            })

        }else {
            props.setAlert({ type: "error", msg: `Enter Valid Data :: [${row.field}]` })
            setRefreshTable(refreshTable + 1)
        }

        setTimeout(() => {
            props.setAlert(undefined)
        }, 6000)
    }
    return (
        <Box sx={{ display: "flex", height: 400, width: '100%' }}>
            {
                <DataGrid
                    rows={(products === undefined) ? [] : products}
                    columns={columns}
                    pageSize={pageSize}
                    onPageSizeChange={(newPageSize) => setPageSize(newPageSize)}
                    rowsPerPageOptions={[10, 15, 20, 25, 30, 35]}
                    pagination
                    disableSelectionOnClick
                    onCellEditCommit={updateCellValue}
                    editMode='cell'
                    // onCellEditStop={updateValue}
                    components={{ Toolbar: GridToolbar }}
                    componentsProps={{
                        toolbar: { showQuickFilter: true },
                    }}
                    loading={(products === undefined) ? true : false}
                    disableExtendRowFullWidth={false}
                    sx={{ width: "100%" }}
                />
            }
        </Box>
    );
}


export default function ReportGeneratorDialog(props: any) {
    const [alert, setAlert] = React.useState<{ type: any, msg: string }>();
    const [openAddProduct, setOpenAddProduct] = React.useState(false);

    return (
        <div>
            <Dialog
                fullWidth={true}
                maxWidth={'lg'}
                open={props.open}
                onClose={() => props.close()}
                aria-labelledby="My Products"
            >
                <DialogTitle id="MyProducts">
                    <Grid container columns={2}>
                        <Grid item sm={1} md={1} xs={1} lg={1}>
                            {"My Products"}
                        </Grid>
                        <Grid item sm={1} md={1} xs={1} lg={1} textAlign="right">
                            <Button variant='outlined' onClick={() => setOpenAddProduct(true)}>Add New</Button>
                        </Grid>
                    </Grid>
                </DialogTitle>
                <Collapse in={(alert === undefined) ? false : true}>
                    {
                        alert && <Alert
                            severity={alert.type}
                            action={
                                <IconButton
                                    aria-label="close"
                                    color="inherit"
                                    size="small"
                                    onClick={() => {
                                        setAlert(undefined);
                                    }}
                                >
                                    <CloseIcon fontSize="inherit" />
                                </IconButton>
                            }
                            sx={{ mb: 2 }}
                        >
                            {alert.msg}
                        </Alert>
                    }
                </Collapse>

                {/* {
                    alert && <Alert severity={alert.type}>{alert.msg}</Alert>
                } */}
                <DialogContent>
                    <ReportGenerator openAddProduct={openAddProduct} setAlert={setAlert} />
                    <DialogContentText>
                        *Double click on cell to update value
                    </DialogContentText>
                </DialogContent>
            </Dialog>
            {
                openAddProduct && <AddProduct open={openAddProduct} setOpen={setOpenAddProduct} alert={alert} />
            }
        </div>
    );
}

