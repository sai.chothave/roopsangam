import React, { useEffect, useState } from 'react'
import Button from '@mui/material/Button';
import Avatar from '@mui/material/Avatar';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import ListItemText from '@mui/material/ListItemText';
import DialogTitle from '@mui/material/DialogTitle';
import Dialog from '@mui/material/Dialog';
import PersonIcon from '@mui/icons-material/Person';
import AddIcon from '@mui/icons-material/Add';
import Typography from '@mui/material/Typography';
import { blue } from '@mui/material/colors';
import { QRCode } from 'react-qrcode-logo';
import rqLogo from "./../../asset/Logo.jpg"
import { DialogContent, Divider, Grid, Zoom } from '@mui/material';
import { TransitionProps } from '@mui/material/transitions';


const Transition = React.forwardRef(function Transition(
    props: TransitionProps & {
      children: React.ReactElement<any, any>;
    },
    ref: React.Ref<unknown>,
  ) {
    return <Zoom in={true} ref={ref} {...props} />;
  });

export const ShowPaymentQR = (props: any) => {
    const { onClose, open, bill } = props;
    const [amount,setAmount] = useState(0);
    const upiID = "saichothave@ybl"

    useEffect(()=>{
        setAmount((bill.discount! > 0)?(bill.amount! - ((Number(bill.discount) / 100) * bill.amount!)).toFixed(0):bill.amount)
    })

    const handleClose = () => {
        onClose();
    };
    return (
        <Dialog onClose={handleClose} open={open} maxWidth={"sm"} TransitionComponent={Transition} keepMounted>
            <div style={{ padding: "10px", backgroundColor: "#00004d" }}>
                <div style={{ border: "1px solid #6666ff" }}>
                    <DialogTitle textAlign={"center"} style={{ backgroundColor: "#00004d", color: "white" }}>Scan To Pay</DialogTitle>
                    <Divider sx={{ height: "1px", background: "#6666ff" }} />
                    <DialogContent sx={{ backgroundColor: "#00004d", color: "white" }}>
                        <Grid container columns={2}>
                            <Grid item xs={1} sm={1} md={1} lg={1} textAlign={"left"}>
                                <Typography fontSize={"0.6rem"} variant='caption'>Bill Number :- { bill.billNumber }</Typography>
                            </Grid>
                            <Grid item xs={1} sm={1} md={1} lg={1} textAlign={"right"}>
                                <Typography fontSize={"0.6rem"} variant='caption'>Amount :- {amount}/-</Typography>
                            </Grid>
                        </Grid>
                        <div style={{}}>
                            <QRCode
                                value={`upi://pay?pa=${upiID}&pn=Roopsangam Dresses&tn=${bill.billNumber}&am=${amount}&cu=INR`}
                                qrStyle={"squares"}
                                eyeColor="#6666ff"
                                eyeRadius={[
                                    {
                                        outer: [10, 10, 0, 10],
                                        inner: [0, 10, 10, 10],
                                    },
                                    [10, 10, 10, 0],
                                    [10, 0, 10, 10],
                                ]}
                                quietZone={20}
                                logoImage={rqLogo}
                                logoHeight={50}
                                logoWidth={50}
                                removeQrCodeBehindLogo={true}
                                ecLevel={"H"}
                                size={200}
                                logoOpacity={1}
                            />
                        </div>
                        <Typography fontFamily={"cursive"} textAlign={"center"}>Roopsangam Dresses</Typography>
                    </DialogContent>
                </div>
            </div>
        </Dialog>
    );
}
