import { Badge, Card, CardActionArea, CardContent, CardMedia, Chip,Fab,  Grid, Typography } from '@mui/material'
import React, { useCallback, useEffect, useState } from 'react'
import { getProducts } from './dataFetcher'
import { IProduct } from './interfaces/product'
import FilterAltIcon from '@mui/icons-material/FilterAlt';
import FilterAltOutlinedIcon from '@mui/icons-material/FilterAltOutlined';
import mensWare from "../../asset/mensWare.jpg"
import womensWare from "../../asset/womensWare.jpg"
import FilterDrawer from './FilterDrawer'
import SearchIcon from '@mui/icons-material/Search';
import { styled, alpha } from '@mui/material/styles';
import InputBase from '@mui/material/InputBase';
import { ProductNotFound } from './ProductNotFound'
import { getProductByCategory, getValuesByFieldName } from './services/product.service'


// import IProducts from './interfaces/product'

const Search = styled('div')(({ theme }) => ({
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: alpha(theme.palette.common.white, 1),
    '&:hover': {
        backgroundColor: alpha(theme.palette.common.white, 1),
    },
    marginLeft: 0,
    width: '100%',
    // [theme.breakpoints.up('sm')]: {
    //     marginLeft: theme.spacing(1),
    //     width: '100%',
    // },
}));

const SearchIconWrapper = styled('div')(({ theme }) => ({
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
    color: 'inherit',
    '& .MuiInputBase-input': {
        padding: theme.spacing(1, 1, 1, 0),
        // vertical padding + font size from searchIcon
        paddingLeft: `calc(1em + ${theme.spacing(4)})`,
        transition: theme.transitions.create('width'),
        width: '100%',
        // [theme.breakpoints.up('sm')]: {
        //     width: '100%',
        //     '&:focus': {
        //         width: '100%',
        //     },
        // },
    },
}));



export const ProductCard = (props: any) => {

    const [products, setProducts] = React.useState<Array<IProduct>>()
    const [filteredProducts, setFilteredProducts] = React.useState<Array<IProduct>>()
    const [filterOpen, setFilterOpen] = useState(false)
    const [filters, setFilters] = React.useState<Array<string>>([]);
    const [availableFilters, setAvailableFilters] = React.useState<Array<string>>([]);

    const addFilters = () => {
        setFilteredProducts(products)
        if (filters.length > 0)
            setFilteredProducts(products?.filter((prod: IProduct) => {
                return filters.indexOf(prod.productSubCategory) >= 0
            }))
    }

    const searchProduct = (e: any) => {
        setFilteredProducts(products)
        setFilteredProducts(products?.filter((prod: IProduct) => {
            if (filters.length > 0)
                return (prod.productName.toUpperCase().indexOf(e.target.value.toUpperCase()) > -1) && (filters.indexOf(prod.productSubCategory) >= 0)
            else
                return prod.productName.toUpperCase().indexOf(e.target.value.toUpperCase()) > -1
        }))

    }

    const getFilters = useCallback(()=>{
        getValuesByFieldName("productSubCategory").then((result)=>setAvailableFilters(result.data)).catch((err)=>alert("Unable to load Filters"))
    },[products])

    useEffect(() => {
        // let data: IProduct[] = getProducts(props.category)

        // getProductByCategory(props.category)?.then((prod) => {
        //     console.log(prod)
        //     setProducts(prod.data)
        //     setFilteredProducts(prod.data)
        // })

        // getProducts(props.category)?.then((prod) => {
        //     console.log(prod)
        //     setProducts(prod)
        //     setFilteredProducts(prod)
        //     setAvailableFilters([])
        // })

        getProductByCategory(props.category).then((prod:any)=>{
            console.log(prod,props.category)
            setProducts(prod.data)
            setFilteredProducts(prod.data)
            getFilters()
        })
        // setProducts(data.products)
        // setFilteredProducts(data.products)
        // setAvailableFilters(data.filters)
    }, [props])


    return (
        <div>
            <Grid container spacing={{ xs: 1, sm: 2, md: 3 }} columns={{ xs: 4, sm: 8, md: 12 }}>
                <Grid item xs={12} sm={12} md={12}>
                    <Search sx={{ zIndex: "10", position: "fixed", bottom: { md: "10px", xs: "55px" }, width: "80%", left: "10%", border: "1px solid #0089ED", boxShadow: " 1px 1px 1px 0px #0089ED", marginBottom: "20px", borderRadius: "30px" }}>
                        <SearchIconWrapper>
                            <SearchIcon />
                        </SearchIconWrapper>
                        <StyledInputBase
                            placeholder="Search Product…"
                            inputProps={{ 'aria-label': 'search Product' }}
                            sx={{ width: "100%" }}
                            onKeyUp={searchProduct}
                        />
                    </Search>
                </Grid>
                {
                    (filteredProducts && filteredProducts.length > 0) ? filteredProducts && filteredProducts.map((product: IProduct, ind: number) =>
                        <Grid item xs={2} sm={4} md={3} key={ind}>
                            <Card elevation={2} sx={{}}>
                                <CardActionArea title={product.productName}>
                                    {/* <CardTitlePopOver title={product.productName}/> */}
                                    <CardMedia
                                        component="img"
                                        height="140"
                                        image={(props.category === "MENS WEAR") ? mensWare : womensWare}
                                        alt={product.productName}
                                    />
                                    <CardContent>
                                        <Typography gutterBottom variant="h5" component="div" sx={{ height: "30px", textOverflow: "ellipsis", whiteSpace: "nowrap", overflow: "hidden" }}>
                                            {product.productName}
                                        </Typography>
                                        <Grid container>
                                            <Grid item>
                                                <div style={{ display: "flex", flexDirection: "row", width: "100%" }}>
                                                    <div>
                                                        <Typography variant="subtitle1" color="text.secondary" component="div">
                                                            {product.productSubCategory}
                                                        </Typography>
                                                        <Typography variant="subtitle1" color="primary.success" component="div">
                                                            <span style={{ color: "green" }}>{`Rs. ${product.productPrice}/-`}</span>
                                                        </Typography>
                                                    </div>
                                                    <div style={{ position: "absolute", right: "5px", top: "5px" }}>
                                                        <Chip label={`${product.inStock} In Stock `} color={(product.inStock === 0) ? "error" : "success"} />
                                                    </div>
                                                </div>
                                            </Grid>
                                            <Grid item sx={{ position: "absolute", right: "10px", display: "flex", flexDirection: "column" }}>
                                                <Chip label={product.productSize} color="primary" variant="outlined" size="small" sx={{ marginBottom: "2px" }} />
                                                {/* <Chip label={product.productCode} color="primary" variant="outlined" /> */}
                                                <span style={{ color: "#baa7a6" }}> [{product.productCode}]</span>
                                            </Grid>
                                        </Grid>
                                    </CardContent>

                                </CardActionArea>

                            </Card>
                        </Grid>
                    ) : <ProductNotFound screen={props.category} />
                }
            </Grid>



            <Fab color="primary" aria-label="add" sx={{ position: "fixed", bottom: { md: "20px", xs: "65px" }, right: "8%" }} onClick={() => { setFilterOpen(true) }}>
                <Badge badgeContent={filters.length} color="primary">
                    {
                        (filters.length > 0) ? <FilterAltIcon /> : <FilterAltOutlinedIcon />
                    }
                </Badge>
            </Fab>


            <FilterDrawer filterOpen={filterOpen} setFilterOpen={setFilterOpen} availableFilters={availableFilters} filters={filters} addFilters={addFilters} setFilters={setFilters} totalResult={filteredProducts && filteredProducts.length} />

        </div>
    )
}
