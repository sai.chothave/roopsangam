import MaleRoundedIcon from '@mui/icons-material/MaleRounded';
import FemaleRoundedIcon from '@mui/icons-material/FemaleRounded';
import DashboardCustomizeRoundedIcon from '@mui/icons-material/DashboardCustomizeRounded';

import React from 'react'

export const Tabs = () => {
    const tabs = [
        {
            "name": "Dashboard", icon: <DashboardCustomizeRoundedIcon />, displayName:"Dashboard"
        },
        {
            "name": "mens wear", icon: <MaleRoundedIcon />, displayName :"Men's Wear"
        },
        {
            "name": "womens wear", icon: <FemaleRoundedIcon />, displayName:"Women's Wear"
        }

    ]
    return tabs;
}