import {  Typography } from '@mui/material'
import Male from "../../asset/maleNotFound.png"
import Female from "../../asset/femaleNotFound.png"

const imgStyle = {
    height: "350px",
    width: "250px"
}

export const ProductNotFound = (props: any) => {
    return (
        <div style={{ height: "250px", width: "250px", margin: "auto", textAlign: "center", padding: "70px 0" }}>
            {
                (props.screen === "MENS WEAR") ? <img src={Male} alt="Product Not Found - Male" style={imgStyle} /> : <img src={Female} alt="Product Not Found - Female" style={imgStyle} />
            }
            <Typography sx={{marginTop:"-50px"}} variant="h6">No Product Found</Typography>
        </div>
    )
}
