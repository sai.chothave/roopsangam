import { useEffect } from 'react'
import styles from './login.module.scss';
import illustration from "../../asset/saly.jpg"
import google from "../../asset/googleLogo.svg"
import facebook from "../../asset/facebook.svg"
import apple from "../../asset/apple.svg"
import profile from "../../asset/profile.png"
import { Link } from '@mui/material';


export const Login = () => {

    useEffect(()=>{

        document.title = "Sign In | Roopsangam"
    })

    return (
        <div className={styles.login}>
            <div className={styles.blueBackground}>
                <div className={styles.logo}>Roopsangam</div>
                <div className={styles.illustration}> <img alt={"Illustration"} src={illustration} /> </div>

                <div className={styles.textCard}>
                    <div className={styles.text}>
                        <ul>
                            <li>Men's Wear</li>
                            <li>Ladies Wear</li>
                            <li>Hojari</li>
                        </ul>
                    </div>
                    <div>
                        <div className={styles.heading}>Sign In to</div>
                        <div className={styles.subHeading}>Roomsangam Dresses</div>
                    </div>
                </div>
            </div>

            <div className={styles.formCard} >
                <div className={styles.card}>
                    <div className={styles.header}>
                        <p className={styles.title}>Welcome to <span style={{ color: "#0089ED" }}>RS</span></p>
                        <div className={styles.navLinks}>
                            No Account ?
                            {/* <a href="#">Sign Up</a> */}
                            <Link href="registration">Sign Up</Link>
                        </div>
                    </div>
                    <div className={styles.cardName}>Sign In</div>
                    <div className={styles.socialLogin}>
                        <Link className={styles.googleLogin} href="#googleLogin">
                            <img className={styles.icon} alt={"Icon"} src={google} />
                            <label className={styles.googleLable}>Sign in with Google</label>
                        </Link>
                        <Link className={styles.facebookLogin}>
                            <img className={styles.icon} alt={"FB-Icon"} style={{marginLeft:"26%", marginTop:"15px", height:"29px"}} src={facebook} />
                        </Link>
                        <Link className={styles.githubLogin}>
                            <img className={styles.icon} alt={"Apple-Icon"} style={{marginLeft:"26%", marginTop:"15px", height:"29px"}} src={apple} />
                        </Link>
                    </div>
                    <form>
                        <div className={styles.input}>
                            <label className={styles.label} htmlFor="username"> Username </label>
                            <input className={styles.inputField} type="text" id="username" name="username" />
                            {/* <p className={styles.error}>Enter valid username</p> */}
                        </div>

                        <div className={styles.input} style={{ marginTop: "134px" }}>
                            <label className={styles.label} htmlFor="password"> Password </label>
                            <input className={styles.inputField} type="text" id="password" name="password" />
                            {/* <p className={styles.error}>Enter valid Password</p> */}
                        </div>


                    </form>

                    <Link href='#' className={styles.fpassLink}>Forgot Password</Link>

                    <button className={styles.button}><span className={styles.text}>Sign in</span></button>

                </div>
            </div>

            <div className={styles.recentLogin} style={{ display: "flex", flexDirection: "row" }}>
                <label className={styles.rlheading}>Login As</label>

                <div>
                    <button className={styles.rlCard}>
                        <img className={styles.profile} alt={"Profile-1"} src={profile}></img>
                        <label className={styles.name}>Sai Chothave</label>
                    </button>
                </div>

                <div>
                    <button className={styles.rlCard}>
                        <img className={styles.profile} alt={"Profile-2"} src={profile}></img>
                        <label className={styles.name}>Sai Chothave</label>
                    </button>
                </div>


            </div>

        </div>
    )
}
