import { useEffect } from 'react'
import styles from './registration.module.scss';
import illustration from "../../asset/saly.jpg"
import profile from "../../asset/profile.png"
import PopupAlert from '../components/PopupAlert';
import { Link } from '@mui/material';
// import illustration from "../../asset/clothStore.png"
// import { style } from '@mui/system';


export const Registration = () => {

    useEffect(()=>{
        document.title = "Sign Up | Roopsangam"
    },[])

    return (
        <div className={styles.login}>
            <title>Registration</title>
            <div className={styles.blueBackground}>
                <div className={styles.logo}>Roopsangam</div>
                <div className={styles.illustration}> <img alt="Illustration" src={illustration} /> </div>

                <div className={styles.textCard}>
                    <div className={styles.text}>
                        <ul>
                            <li>Men's Wear</li>
                            <li>Ladies Wear</li>
                            <li>Hojari</li>
                        </ul>
                    </div>
                    <div>
                        <div className={styles.heading}>Sign Up to</div>
                        <div className={styles.subHeading}>Roomsangam Dresses</div>
                    </div>
                </div>
            </div>

            <div className={styles.formCard} >
                <div className={styles.card}>
                    {/* <Alert severity='error'  sx={{position:"absolute", width:"75%",left:"8%", top:"5px", zIndex:"1"}}>Registration Successfull</Alert> */}
                    <PopupAlert type={"error"} msg={"Invalid Details"}/>
                    <div className={styles.header}>
                        <p className={styles.title}>Welcome to <span style={{ color: "#0089ED" }}>RS</span></p>
                        <div className={styles.navLinks}>
                            Have an Account ?
                            {/* <a href="#">Sign in</a> */}
                            <Link href='/'>Sign in</Link>
                        </div>
                    </div>
                    <div className={styles.cardName}>Sign Up</div>
                    {/* <div className={styles.socialLogin}>
                        <a className={styles.googleLogin} href="#googleLogin">
                            <img className={styles.icon} src={google} />
                            <label className={styles.googleLable}>Sign up with Google</label>
                        </a>
                        <div className={styles.facebookLogin}></div>
                        <div className={styles.githubLogin}></div>
                    </div> */}
                    <form style={{ display: "flex", flexDirection: "column" }}>
                        <div className={styles.input}>
                            <label className={styles.label} htmlFor="name"> Enter your name </label>
                            <input className={styles.inputField} type="text" id="name" name="name" />
                            {/* <p className={styles.error}>Enter valid username</p> */}
                        </div>

                        <div style={{ display: "flex", flexDirection: "row", width: "85%" }}>
                            <div className={styles.input} style={{ width: "50%", marginRight: "10px" }}>
                                <label className={styles.label} htmlFor="username"> Username </label>
                                <input className={styles.inputField} type="text" id="username" name="username" />
                                {/* <p className={styles.error}>Enter valid username</p> */}
                            </div>
                            <div className={styles.input} style={{ width: "50%", marginLeft: "10px" }}>
                                <label className={styles.label} htmlFor="contact"> Mobile </label>
                                <input className={styles.inputField} type="number" id="contact" name="contact" />
                                {/* <p className={styles.error}>Enter valid username</p> */}
                            </div>
                        </div>

                        <div className={styles.input}>
                            <label className={styles.label} htmlFor="password"> Password </label>
                            <input className={styles.inputField} type="text" id="password" name="password" />
                            {/* <p className={styles.error}>Enter valid Password</p> */}
                        </div>

                        {/* <a href='#' className={styles.fpassLink}>Forgot Password</a> */}
                        <Link href='#' className={styles.fpassLink} >Forgot Password</Link>

                        <button type='submit' className={styles.button}><span className={styles.text}>Register</span></button>
                    </form>
                </div>
            </div>

            <div className={styles.recentLogin} style={{ display: "flex", flexDirection: "row" }}>
                <label className={styles.rlheading}>Login As</label>

                <div>
                    <button className={styles.rlCard}>
                        <img className={styles.profile} alt={"Profile-1"} src={profile} />
                        <label className={styles.name}>Sai Chothave</label>
                    </button>
                </div>

                <div>
                    <button className={styles.rlCard}>
                        <img className={styles.profile} alt={"Profile-2"} src={profile} />
                        <label className={styles.name}>Sai Chothave</label>
                    </button>
                </div>
            </div>

        </div>
    )
}
