import './App.css';
// import { Login } from './Pages/LoginPage/Login';
// import { Registration } from './Pages/RegistrationPage/Registration';
import { DashboardPage } from './Pages/DashboardPage/DashboardPage';

import { ThemeProvider, createTheme } from '@mui/material/styles';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { Login } from './Pages/LoginPage/Login';
import { Registration } from './Pages/RegistrationPage/Registration';
import DelayPrintingInformation from './Dardan/DelayPrintingInformation';
import EditDelayPrinting from './Dardan/EditDelayPrinting';
import { EDPDriver } from './Dardan/EDPDriver';
import { createContext, useContext, useEffect, useState } from 'react';
import { MobileScanner } from './Pages/components/Scanner/MobileScanner';
import { io } from 'socket.io-client';


const darkTheme = createTheme({
  palette: {
    mode: 'light',
  },
});

interface WindowContextInterface {
  window: "billingWindow" | "reportWindow" | "billingReportWindow" | "newProductWindow" | false;
  setWindow: any
}

const getHost = () => {
  return window.location.hostname;
}

export const WindowContext = createContext<WindowContextInterface|null>(null);
const socket = io(`https://${getHost()}:8081/`,{ secure: false, reconnection: true, rejectUnauthorized: false});
export const socketContext = createContext(socket);

function App() {

  const [window, setWindow] = useState<"billingWindow" | "reportWindow" | "billingReportWindow" | "newProductWindow" | false>(false);
  

  const OnKeyDown = (e: KeyboardEvent) => {
    if (e.ctrlKey && e.keyCode === 66) {
      setWindow("billingWindow")
    } else if (e.shiftKey && e.keyCode === 82) {
      setWindow("reportWindow")
    } else if (e.shiftKey && e.keyCode === 66) {
      setWindow("billingReportWindow")
    }
  }

  useEffect(() => {
    document.addEventListener('keydown', OnKeyDown)
    socket.connect()
  }, [])

  return (
    <WindowContext.Provider value={{ window: window, setWindow: setWindow }}>
      <socketContext.Provider value={socket}>
      <ThemeProvider theme={darkTheme}>
        {/* <div>
        <DashboardPage />
        <Registration/>
        <Login/>
      </div> */}
        <BrowserRouter>
          <Routes>
            <Route path="/login" element={<Login />} />
            <Route path="registration" element={<Registration />} />
            <Route path="/" element={<DashboardPage />} />
            <Route path="/*" element={<p>404 Page Not Found</p>} />
            <Route path="edp" element={<EditDelayPrinting />} />
            <Route path="/scanner" element={<MobileScanner/>}/>
            <Route path="edpdriver" element={<EDPDriver />} />
          </Routes>
        </BrowserRouter>
      </ThemeProvider>
      </socketContext.Provider>
    </WindowContext.Provider>
  );
}

export default App;
