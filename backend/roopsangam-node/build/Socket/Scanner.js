"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.registerScannerHandler = void 0;
const registerScannerHandler = (io, socket) => {
    socket.on("isScannerOnline", () => __awaiter(void 0, void 0, void 0, function* () {
        var _a, _b;
        if (((_a = io.sockets.adapter.rooms.get("scanner")) === null || _a === void 0 ? void 0 : _a.size) !== undefined || ((_b = io.sockets.adapter.rooms.get("scanner")) === null || _b === void 0 ? void 0 : _b.size) === 1) {
            socket.to("desktop").emit("alert", "success", "Scanner Online");
        }
        else {
            socket.to("desktop").emit("alert", "error", "Scanner Offline");
        }
    }));
    socket.on("openBillingWindow", () => __awaiter(void 0, void 0, void 0, function* () {
        socket.to("desktop").emit("openBillingWindow");
    }));
    socket.on("getBillingWindowStatus", () => {
        console.log("gettingSTS");
        socket.to("desktop").emit("getBillingWindowStatus");
    });
    socket.on("billingWindowStatus", (status) => {
        socket.to("scanner").emit("billingWindowStatus", status);
    });
    socket.on("addProduct", (productId) => {
        socket.to("desktop").emit("addProduct", productId);
    });
};
exports.registerScannerHandler = registerScannerHandler;
