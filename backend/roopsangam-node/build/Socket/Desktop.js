"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.registerDesktopHandler = void 0;
const registerDesktopHandler = (io, socket) => {
    socket.on("isDesktopOnline", () => __awaiter(void 0, void 0, void 0, function* () {
        var _a, _b;
        if (((_a = io.sockets.adapter.rooms.get("desktop")) === null || _a === void 0 ? void 0 : _a.size) !== undefined || ((_b = io.sockets.adapter.rooms.get("desktop")) === null || _b === void 0 ? void 0 : _b.size) === 1) {
            socket.to("scanner").emit("alert", "success", "System Online");
        }
        else {
            socket.to("scanner").emit("alert", "error", "System Offline");
        }
    }));
    socket.on("billingWindowClosed", () => {
        socket.to("scanner").emit("billingWindowClosed");
    });
    socket.on("billingWindowOpened", () => {
        socket.to("scanner").emit("billingWindowOpened");
    });
    socket.on("productAdded", (products) => {
        socket.to("scanner").emit("productAdded", products);
    });
    socket.on("errorWhileAddingProduct", (err) => {
        socket.to("scanner").emit("errorWhileAddingProduct", err);
    });
    socket.on("acceptOnlinePayment", (bill) => {
        socket.to("scanner").emit("acceptOnlinePayment", bill);
    });
};
exports.registerDesktopHandler = registerDesktopHandler;
