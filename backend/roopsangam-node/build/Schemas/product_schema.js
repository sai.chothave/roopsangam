"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Product = void 0;
const mongoose_1 = __importDefault(require("mongoose"));
const productSchema = new mongoose_1.default.Schema({
    productName: {
        require: true,
        type: String,
    },
    productCategory: {
        require: true,
        type: String,
    },
    productSubCategory: {
        require: true,
        type: String,
    },
    productPrice: {
        require: true,
        type: Number,
    },
    inStock: {
        require: true,
        type: Number,
        min: 0
    },
    productCode: {
        type: String,
    },
    productSize: {
        require: true,
        type: String,
    },
});
productSchema.index({ "productName": 1, "productCategory": 1, "productSize": 1, "productSubCategory": 1 }, { unique: true });
const Product = mongoose_1.default.model("Product", productSchema);
exports.Product = Product;
