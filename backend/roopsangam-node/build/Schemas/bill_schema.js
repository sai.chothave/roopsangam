"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Bill = void 0;
const mongoose_1 = __importDefault(require("mongoose"));
// const purchedProducts = new Product()
// purchedProducts.add({
//     buyQuantity:{type:Number}
// })
const billSchema = new mongoose_1.default.Schema({
    billNumber: { type: Number, require: true, unique: true },
    products: [{
            product: {
                type: mongoose_1.default.Schema.Types.ObjectId,
                ref: 'Product'
            },
            buyQuantity: Number
        }],
    paymentMode: { type: String, require: true, default: "Cash" },
    date: { type: Date, require: true, default: Date.now },
    time: { type: String, require: true },
    amount: { type: Number, require: true },
    discount: { type: Number, require: true },
    status: { type: String, require: true, default: "Paid" }
});
// billSchema.index({"productName":1,"productCategory":1,"productSize":1},{unique:true})
const Bill = mongoose_1.default.model("Bill", billSchema, "bills");
exports.Bill = Bill;
