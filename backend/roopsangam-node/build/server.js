"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const dotenv_1 = __importDefault(require("dotenv"));
const body_parser_1 = require("body-parser");
const endpoints_1 = require("./endpoints");
const cors_1 = __importDefault(require("cors"));
const product_1 = require("./Endpoints/product");
const bill_1 = require("./Endpoints/bill");
const MongoConnection_1 = require("./MongoConnection");
const socket_io_1 = require("socket.io");
const Scanner_1 = require("./Socket/Scanner");
const Desktop_1 = require("./Socket/Desktop");
const https_1 = require("https");
const fs_1 = require("fs");
dotenv_1.default.config();
const port = process.env.PORT;
const mongoURI = process.env.MONGO_URL || "";
const app = (0, express_1.default)();
app.use((0, body_parser_1.json)());
app.use((0, cors_1.default)());
MongoConnection_1.conn.get("roopsangam");
// mongoose.connect("mongodb+srv://sm4989:Sam_4989@cluster0.wemnv.mongodb.net/roopsangam?retryWrites=true&w=majority", {
//     useNewUrlParser: true,
//     useUnifiedTopology: true,
// } as ConnectOptions, () => {
//     console.log(mongoURI)
//     console.log("Connected to Database")
// })
app.use(endpoints_1.Endpoints);
app.use("/product", product_1.ProductEndpoints);
app.use("/billing", bill_1.BillingEndpoints);
const server = app.listen(port, () => {
    console.log(`[Roopsangam - server]: Server is running at https://localhost:${port}`);
});
// const io = new Server<ClientToServerEvents, ServerToClientEvents, InterServerEvents, SocketData>({ cors: { origin: "*" } });
// console.log("file",readFileSync("./SSL/key.pem"))
const httpServer = (0, https_1.createServer)({
    key: (0, fs_1.readFileSync)(".\/src\/SSL\/key1.pem"),
    cert: (0, fs_1.readFileSync)(".\/src\/SSL\/cert4.pem")
});
const io = new socket_io_1.Server(httpServer, { cors: { origin: "*" }, pingTimeout: 2500, pingInterval: 5000 });
const onJoin = (data, socket) => __awaiter(void 0, void 0, void 0, function* () {
    var _a, _b;
    if (((_a = io.sockets.adapter.rooms.get(data.deviceType)) === null || _a === void 0 ? void 0 : _a.size) === undefined || ((_b = io.sockets.adapter.rooms.get(data.deviceType)) === null || _b === void 0 ? void 0 : _b.size) < 1) {
        if (data.deviceType === "desktop") {
            yield socket.join("desktop");
            socket.data.deviceType = "desktop";
            socket.to("scanner").emit("alert", "success", "System Online");
        }
        else if (data.deviceType === "scanner" && !socket.rooms.has("scanner")) {
            yield socket.join("scanner");
            socket.data.deviceType = "scanner";
            socket.to("desktop").emit("alert", "success", "Scanner Online");
        }
    }
    else {
        console.log("Already Connected");
        socket.emit("alert", "error", `${data.deviceType} is already connected`);
        socket.disconnect(true);
    }
});
const onConnection = (socket) => {
    socket.on("join", (data) => { onJoin(data, socket); });
    (0, Desktop_1.registerDesktopHandler)(io, socket);
    (0, Scanner_1.registerScannerHandler)(io, socket);
    socket.on("disconnecting", (reason) => {
        if (reason === "ping timeout") {
            socket.disconnect(true);
            socket.to((socket.data.deviceType === "scanner") ? "desktop" : "scanner").emit("alert", "warning", `${socket.data.deviceType} is Disconnected`);
        }
    });
    socket.on("disconnect", (reason) => {
        if (reason === "transport close") {
            socket.disconnect(true);
            socket.to((socket.data.deviceType === "scanner") ? "desktop" : "scanner").emit("alert", "error", `${socket.data.deviceType} is Disconnected`);
        }
    });
};
io.on("connection", onConnection);
httpServer.listen(8081).on("error", (error) => {
    console.log(error);
});
// io.on("connection", (socket) => {
//     socket.on("join", async (data: any) => {
//         if (io.sockets.adapter.rooms.get(data.deviceType)?.size === undefined || io.sockets.adapter.rooms.get(data.deviceType)?.size! < 1) {
//             if (data.deviceType === "desktop") {
//                 await socket.join("desktop")
//                 socket.data.deviceType = "desktop"
//                 socket.to("scanner").emit("alert", "success", "System Online")
//             } else if (data.deviceType === "scanner" && !socket.rooms.has("scanner")) {
//                 await socket.join("scanner")
//                 socket.data.deviceType = "scanner"
//                 socket.to("desktop").emit("alert", "success", "Scanner Online")
//             }
//         } else {
//             console.log("Already Connected")
//             socket.emit("alert", "error", `${data.deviceType} is already connected`)
//             socket.disconnect(true);
//         }
//     });
//     socket.on("isScannerOnline", async () => {
//         if (io.sockets.adapter.rooms.get("scanner")?.size !== undefined || io.sockets.adapter.rooms.get("scanner")?.size === 1) {
//             socket.to("desktop").emit("alert", "success", "Scanner Online")
//         } else {
//             socket.to("desktop").emit("alert", "error", "Scanner Offline")
//         }
//     })
//     socket.on("isDesktopOnline", async () => {
//         if (io.sockets.adapter.rooms.get("desktop")?.size !== undefined || io.sockets.adapter.rooms.get("desktop")?.size === 1) {
//             socket.to("scanner").emit("alert", "success", "System Online")
//         } else {
//             socket.to("scanner").emit("alert", "error", "System Offline")
//         }
//     })
//     socket.on("disconnect", async (reason: any) => {
//         if (reason === "transport close")
//             socket.to("desktop").emit("alert", "error", "Scanner Offline")
//     })
//     socket.on("openBillingWindow", async () => {
//         socket.to("desktop").emit("openBillingWindow")
//     })
//     socket.on("error", (error: Error) => {
//         console.log(error);
//         socket.emit("alert", "error", `${error}`)
//     })
// });
