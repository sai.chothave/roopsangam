"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Endpoints = void 0;
const express_1 = __importDefault(require("express"));
const user_1 = require("./Schemas/user");
const bcrypt_1 = __importDefault(require("bcrypt"));
const router = express_1.default.Router();
exports.Endpoints = router;
router.get("/", (req, res) => {
    res.status(200).send("Server is Running...!");
});
router.post("/login", (req, res) => {
    const { username, password } = req.body;
    if (username === undefined)
        return res.status(400).send("Username Required");
    else if (password === undefined)
        return res.status(400).send("Password Required");
    else {
        user_1.User.findOne({ username: username }).then((data) => {
            if (!data)
                return res.status(404).send("Invalid Username");
            bcrypt_1.default.compare(password, (data === null || data === void 0 ? void 0 : data.password) || "").then(function (result) {
                if (result)
                    return res.status(200).send(data);
                else
                    return res.status(404).send("Invalid Password");
            });
        }).catch((err) => {
            return res.status(500).send("Error => " + err);
        });
    }
});
router.post("/register", (req, res) => {
    const user = req.body;
    if (user.username === undefined)
        return res.status(400).send("Username Required");
    else if (user.password === undefined)
        return res.status(400).send("Password Required");
    else if (user.email === undefined)
        return res.status(400).send("Email Required");
    else {
        bcrypt_1.default.hash(user.password, 10, function (err, hash) {
            return __awaiter(this, void 0, void 0, function* () {
                if (err)
                    return res.status(500).send("Error => " + err);
                else {
                    user.password = hash;
                    user_1.User.find({ username: user.username }).then((data) => {
                        if (data.length > 0) {
                            return res.status(404).send("Username Already exist");
                        }
                        else {
                            new user_1.User(Object.assign({}, user)).save().then(() => {
                                return res.status(200).send("User Registered Successfully...!");
                            }).catch((err) => {
                                return res.status(500).send("Error => " + err);
                            });
                        }
                    });
                }
            });
        });
    }
});
