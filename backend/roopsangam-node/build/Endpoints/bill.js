"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BillingEndpoints = void 0;
const express_1 = __importDefault(require("express"));
const MongoConnection_1 = require("../MongoConnection");
const bill_schema_1 = require("../Schemas/bill_schema");
const product_schema_1 = require("../Schemas/product_schema");
const UniqueIdGenerator_1 = __importDefault(require("./UniqueIdGenerator"));
const router = express_1.default.Router();
exports.BillingEndpoints = router;
const convertBillIntoRequiredFormat = (bill) => {
    let products = bill.products.map((prod) => {
        return Object.assign({ buyQuantity: prod.buyQuantity }, prod.product.toObject());
    });
    return Object.assign(Object.assign({}, bill.toObject()), { products: products });
};
router.get("/", (req, res) => {
    bill_schema_1.Bill.find().populate("products.product").then((result) => {
        return res.status(200).send(result.map((bill) => convertBillIntoRequiredFormat(bill)));
    }).catch((err) => {
        res.status(500).send(err);
    });
});
router.get("/:N", (req, res) => {
    bill_schema_1.Bill.find().populate("products.product").sort({ _id: -1 }).limit(Number(req.params.N)).then((result) => {
        return res.status(200).send(result.map((bill) => convertBillIntoRequiredFormat(bill)));
    }).catch((err) => {
        console.log(err);
        res.status(500).send(err);
    });
});
// router.post("/", (req: Request, res: Response) => {
//     let bill: IBill = req.body
//     getValueForNextSequence("billNumber").then((data) => {
//         if (data) {
//             bill.billNumber = data?.seq
//             Product.find({}, { "_id": 1 }).then((result) => {
//                 // Fetched All Products and check in buyed products are available in DB or Not
//                 result = result.map((res) => res.id)
//                 // ids = bill.products.filter((prod)=>result.indexOf(String(prod.product))<0).reduce((p,c)=>p+=c!.product+", ","")
//                 let ids = bill.products.reduce((prev, curr) => {
//                     if (result.indexOf(curr.product) == -1)
//                         return prev += curr.product + ", "
//                     else
//                         return prev += ""
//                 }, "")
//                 if (ids.length > 0)
//                     return res.status(400).send("Invalid Products :: " + ids)
//                 else {
//                     new Bill({ ...bill }).save().then((result) => {
//                         result.populate("products.product").then((newBill) => {
//                             let formatedBill = convertBillIntoRequiredFormat(newBill)
//                             // return res.status(200).send({billNumber:newBill.billNumber,amount:newBill.amount,date:newBill.date,discount:newBill.discount,paymentMode:newBill.paymentMode,time:newBill.time,status:newBill.status,_id:newBill._id,products:products})
//                             console.log(formatedBill.products)
//                             return res.status(200).send(formatedBill)
//                         }).catch((err) => {
//                             return res.status(503).send("Unable to fetch Generated Bill [ bill Number :: " + result.billNumber)
//                         })
//                     }).catch((err) => {
//                         console.log(err)
//                         return res.status(500).send(err)
//                     })
//                 }
//             }).catch((err) => {
//                 return res.status(503).send("Unable to process request")
//             })
//         }
//     }).catch((Err) => {
//         res.status(503).send("Unable to generate Bill Number")
//     })
// })
// router.post("/bill", (req: Request, res: Response) => {
//     let bill: IBill = req.body
//     getValueForNextSequence("billNumber").then((data) => {
//         if (data) {
//             bill.billNumber = data?.seq
//             Product.find({}, { "_id": 1 }).then((result) => {
//                 // Fetched All Products and check in buyed products are available in DB or Not
//                 result = result.map((res) => res.id)
//                 // ids = bill.products.filter((prod)=>result.indexOf(String(prod.product))<0).reduce((p,c)=>p+=c!.product+", ","")
//                 let ids = bill.products.reduce((prev, curr) => {
//                     if (result.indexOf(curr.product) == -1)
//                         return prev += curr.product + ", "
//                     else
//                         return prev += ""
//                 }, "")
//                 if (ids.length > 0)
//                     return res.status(400).send("Invalid Products :: " + ids)
//                 else {
//                     new Bill({ ...bill }).save().then((result) => {
//                         result.populate("products.product").then((newBill) => {
//                             let formatedBill = convertBillIntoRequiredFormat(newBill)
//                             // return res.status(200).send({billNumber:newBill.billNumber,amount:newBill.amount,date:newBill.date,discount:newBill.discount,paymentMode:newBill.paymentMode,time:newBill.time,status:newBill.status,_id:newBill._id,products:products})
//                             console.log(formatedBill)
//                             let promises = formatedBill.products.map((prod: any) => {
//                                 return Product.findByIdAndUpdate(prod._id, { inStock: prod.inStock - prod.buyQuantity })
//                             })
//                             Promise.all(promises).then((val) => {
//                                 console.log(val)
//                                 return res.status(200).send(formatedBill)
//                             }).catch((e) => console.log("ErrorWhileupdaation", e))
//                         }).catch((err) => {
//                             return res.status(503).send("Unable to fetch Generated Bill [ bill Number :: " + result.billNumber)
//                         })
//                     }).catch((err) => {
//                         console.log(err)
//                         return res.status(500).send(err)
//                     })
//                 }
//             }).catch((err) => {
//                 return res.status(503).send("Unable to process request")
//             })
//         }
//     }).catch((Err) => {
//         res.status(503).send("Unable to generate Bill Number")
//     })
// })
const generateBill = (bill, res) => __awaiter(void 0, void 0, void 0, function* () {
    const session = yield MongoConnection_1.conn.startSession();
    let billOperation = { msg: "", status: 0 };
    try {
        session.startTransaction();
        yield (0, UniqueIdGenerator_1.default)("billNumber", session).then((data) => __awaiter(void 0, void 0, void 0, function* () {
            if (data) {
                bill.billNumber = data === null || data === void 0 ? void 0 : data.seq;
                yield new bill_schema_1.Bill(Object.assign({}, bill)).save({ session }).then((result) => __awaiter(void 0, void 0, void 0, function* () {
                    console.log(result);
                    result.populate("products.product").then((newBill) => {
                        let formatedBill = convertBillIntoRequiredFormat(newBill);
                        let promises = formatedBill.products.map((prod) => {
                            if (prod.inStock - prod.buyQuantity < 0)
                                throw `Insufficient Quantity of ${prod.productName}-${prod.productSize}`;
                            return product_schema_1.Product.findByIdAndUpdate(prod._id, { inStock: prod.inStock - prod.buyQuantity }, { session });
                        });
                        Promise.all(promises).then((val) => __awaiter(void 0, void 0, void 0, function* () {
                            // throw "InvalidError"
                            yield session.commitTransaction();
                            billOperation = { status: 200, msg: formatedBill };
                            return res.status(200).send(formatedBill);
                        })).catch((err) => __awaiter(void 0, void 0, void 0, function* () {
                            console.log("ErrorWhileUpdating inStock");
                            yield session.abortTransaction();
                            billOperation = { status: 409, msg: "Error while updating inStock" };
                            return res.status(409).send("Error while updating inStock");
                        }));
                    }).catch((err) => __awaiter(void 0, void 0, void 0, function* () {
                        // return res.status(503).send("Unable to fetch Generated Bill [ bill Number :: " + result.billNumber)
                        console.log("ErrorWhileFetchingBill", err);
                        yield session.abortTransaction();
                        if (err.indexOf("Insufficient") !== -1)
                            return res.status(422).send(err);
                        billOperation = { status: 503, msg: "Unable to fetch Generated Bill [ bill Number :: " + result.billNumber };
                        return res.status(503).send("Unable to fetch Generated Bill [ bill Number :: " + result.billNumber);
                    }));
                })).catch((err) => __awaiter(void 0, void 0, void 0, function* () {
                    console.log("ErrorWhileSavingBill");
                    yield session.abortTransaction();
                    console.log(err);
                    // return res.status(500).send(err)
                    billOperation = { status: 403, msg: err };
                    return res.status(403).send(err);
                }));
            }
        })).catch((err) => __awaiter(void 0, void 0, void 0, function* () {
            console.log("ErrorWhileGeneratingNewBillNumber");
            yield session.abortTransaction();
            billOperation = { status: 503, msg: "Unable to generate Bill Number" };
            return res.status(503).send("Unable to generate Bill Number");
        }));
    }
    catch (err) {
        yield session.abortTransaction();
        billOperation = { status: 403, msg: err };
        return res.status(403).send(err);
    }
});
const returnBill = (bill, res) => __awaiter(void 0, void 0, void 0, function* () {
    const session = yield MongoConnection_1.conn.startSession();
    try {
        session.startTransaction();
        let promises = bill.products.map((prod) => {
            return product_schema_1.Product.findByIdAndUpdate(prod._id, { inStock: prod.inStock + prod.buyQuantity }, { session });
        });
        Promise.all(promises).then((val) => __awaiter(void 0, void 0, void 0, function* () {
            bill_schema_1.Bill.findOneAndUpdate({ billNumber: bill.billNumber }, { status: "Return Accepted" }).then((result) => __awaiter(void 0, void 0, void 0, function* () {
                if (result) {
                    yield session.commitTransaction();
                    return res.status(200).send(`Return Accepted Successfully [ Bill Number :: ${bill.billNumber} ]`);
                }
                else {
                    console.log("Bill Not found");
                    yield session.abortTransaction();
                    return res.send("Bill Not found").status(404);
                }
            })).catch((err) => __awaiter(void 0, void 0, void 0, function* () {
                console.log("ErrorWhileUpdating Bill Status", err);
                yield session.abortTransaction();
                return res.status(409).send("Error while updating Bill status");
            }));
        })).catch((err) => __awaiter(void 0, void 0, void 0, function* () {
            console.log("ErrorWhileUpdating inStock");
            yield session.abortTransaction();
            return res.status(409).send("Error while updating inStock");
        }));
    }
    catch (err) {
        yield session.abortTransaction();
        return res.status(403).send(err);
    }
});
router.patch("/", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const bill = req.body;
    if (bill === undefined)
        return res.send(403).send("Invalid Bill");
    else
        yield returnBill(bill, res);
}));
router.post("/", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    let bill = req.body;
    yield generateBill(bill, res);
}));
