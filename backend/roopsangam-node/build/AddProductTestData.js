"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AddProductsTestData = void 0;
const faker_1 = require("@faker-js/faker");
const product_schema_1 = require("../Schemas/product_schema");
const AddProductsTestData = () => {
    const allProducts = [];
    for (let i = 0; i < 500; i++) {
        const product = { productName: faker_1.faker.commerce.product().toLocaleUpperCase(), productCategory: faker_1.faker.helpers.arrayElement(["mens wear", "womens wear"]).toLocaleUpperCase(), inStock: faker_1.faker.helpers.arrayElement([1, 0, 500, 20, 100, 50, 1000, 40]), productCode: "HEK", productPrice: Number(faker_1.faker.commerce.price()), productSize: faker_1.faker.helpers.arrayElement(["S", "M", "L", "XL", "XXL", "30", "32", "34", "36", "38"]), productSubCategory: faker_1.faker.commerce.productAdjective().toLocaleUpperCase(), }, allProducts, push;
        (product);
    }
    product_schema_1.Product.insertMany(allProducts).then((res) => {
        console.log(res.length, 'Products Added');
    }).catch((err) => {
        console.log('Error While Adding Products');
    });
};
exports.AddProductsTestData = AddProductsTestData;
(0, exports.AddProductsTestData)();
