import express, { Request, Response } from "express";
import { User } from "./Schemas/user";
import bcrypt from "bcrypt";

const router = express.Router();

router.get("/",(req,res)=>{
    res.status(200).send("Server is Running...!")
})


router.post("/login", (req: Request, res: Response) => {
    const {username, password} = req.body
    if (username === undefined)
        return res.status(400).send("Username Required")
    else if (password === undefined)
        return res.status(400).send("Password Required")
    else{
        User.findOne({ username: username }).then((data) => {
            if(!data)
                return res.status(404).send("Invalid Username")
            bcrypt.compare(password, data?.password || "").then(function(result) {
                if(result)
                    return res.status(200).send(data)
                else
                    return res.status(404).send("Invalid Password")
            });
        }).catch((err)=>{
            return res.status(500).send("Error => "+err)
        })
    }
})

router.post("/register", (req: Request, res: Response) => {
    const user: IUser = req.body
    if (user.username === undefined)
        return res.status(400).send("Username Required")
    else if (user.password === undefined)
        return res.status(400).send("Password Required")
    else if (user.email === undefined)
        return res.status(400).send("Email Required")
    else {
        bcrypt.hash(user.password, 10, async function (err, hash) {
            if (err)
                return res.status(500).send("Error => " + err)
            else {
                user.password = hash
                User.find({ username: user.username }).then((data) => {
                    if (data.length > 0) {
                        return res.status(404).send("Username Already exist")
                    }
                    else {
                        new User({ ...user }).save().then(() => {
                            return res.status(200).send("User Registered Successfully...!")
                        }).catch((err) => {
                            return res.status(500).send("Error => " + err)
                        })
                    }
                })
            }
        })
    }
})

export { router as Endpoints }