import mongoose, { ConnectOptions } from "mongoose";

mongoose.connect("mongodb+srv://saichothave:Roopsangam@cluster0.3qjtwtr.mongodb.net/?retryWrites=true&w=majority", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
} as ConnectOptions).catch((err)=>{
    console.log("Error While connecting to MongoDB => "+err)
})

const conn = mongoose.connection;

conn.on('error', () => console.error.bind(console, 'connection error'));

conn.once('open', () => console.info('Connection to Database is successful'));

// module.exports = conn;
export {conn}