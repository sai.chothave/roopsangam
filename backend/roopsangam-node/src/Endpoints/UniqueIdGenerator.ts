import { ClientSession } from "mongoose";
import { resolve } from "path";
import { Counter } from "../Schemas/counter";

export default async function getValueForNextSequence(sequenceOfName: String,session:ClientSession|null=null) {
    return Counter.findOneAndUpdate({ field: sequenceOfName }, { $inc: { seq: 1 } },{session})
}