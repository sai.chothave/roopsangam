import express, { Request, response, Response } from "express";
import mongoose from "mongoose";
import { conn } from "../MongoConnection";
import { Bill } from "../Schemas/bill_schema";
import { Product } from "../Schemas/product_schema";
import { IBill } from "./../Interfaces/bill"
import getValueForNextSequence from "./UniqueIdGenerator";


const router = express.Router();

const convertBillIntoRequiredFormat = (bill: any) => {
    let products = bill.products.map((prod: any) => {
        return { buyQuantity: prod.buyQuantity, ...prod.product!.toObject() }
    })
    return { ...bill.toObject(), products: products }
}

router.get("/", (req: Request, res: Response) => {
    Bill.find().populate("products.product").then((result) => {
        return res.status(200).send(result.map((bill) => convertBillIntoRequiredFormat(bill)))
    }).catch((err) => {
        res.status(500).send(err)
    })
})

router.get("/:N", (req: Request, res: Response) => {
    Bill.find().populate("products.product").sort({ _id: -1 }).limit(Number(req.params.N)).then((result) => {
        return res.status(200).send(result.map((bill) => convertBillIntoRequiredFormat(bill)))
    }).catch((err) => {
        console.log(err)
        res.status(500).send(err)
    })
})

// router.post("/", (req: Request, res: Response) => {
//     let bill: IBill = req.body
//     getValueForNextSequence("billNumber").then((data) => {
//         if (data) {
//             bill.billNumber = data?.seq
//             Product.find({}, { "_id": 1 }).then((result) => {
//                 // Fetched All Products and check in buyed products are available in DB or Not
//                 result = result.map((res) => res.id)
//                 // ids = bill.products.filter((prod)=>result.indexOf(String(prod.product))<0).reduce((p,c)=>p+=c!.product+", ","")
//                 let ids = bill.products.reduce((prev, curr) => {
//                     if (result.indexOf(curr.product) == -1)
//                         return prev += curr.product + ", "
//                     else
//                         return prev += ""
//                 }, "")
//                 if (ids.length > 0)
//                     return res.status(400).send("Invalid Products :: " + ids)
//                 else {
//                     new Bill({ ...bill }).save().then((result) => {
//                         result.populate("products.product").then((newBill) => {
//                             let formatedBill = convertBillIntoRequiredFormat(newBill)
//                             // return res.status(200).send({billNumber:newBill.billNumber,amount:newBill.amount,date:newBill.date,discount:newBill.discount,paymentMode:newBill.paymentMode,time:newBill.time,status:newBill.status,_id:newBill._id,products:products})
//                             console.log(formatedBill.products)

//                             return res.status(200).send(formatedBill)
//                         }).catch((err) => {
//                             return res.status(503).send("Unable to fetch Generated Bill [ bill Number :: " + result.billNumber)
//                         })
//                     }).catch((err) => {
//                         console.log(err)
//                         return res.status(500).send(err)
//                     })
//                 }
//             }).catch((err) => {
//                 return res.status(503).send("Unable to process request")
//             })
//         }
//     }).catch((Err) => {
//         res.status(503).send("Unable to generate Bill Number")
//     })

// })

// router.post("/bill", (req: Request, res: Response) => {
//     let bill: IBill = req.body
//     getValueForNextSequence("billNumber").then((data) => {
//         if (data) {
//             bill.billNumber = data?.seq
//             Product.find({}, { "_id": 1 }).then((result) => {
//                 // Fetched All Products and check in buyed products are available in DB or Not
//                 result = result.map((res) => res.id)
//                 // ids = bill.products.filter((prod)=>result.indexOf(String(prod.product))<0).reduce((p,c)=>p+=c!.product+", ","")
//                 let ids = bill.products.reduce((prev, curr) => {
//                     if (result.indexOf(curr.product) == -1)
//                         return prev += curr.product + ", "
//                     else
//                         return prev += ""
//                 }, "")
//                 if (ids.length > 0)
//                     return res.status(400).send("Invalid Products :: " + ids)
//                 else {
//                     new Bill({ ...bill }).save().then((result) => {
//                         result.populate("products.product").then((newBill) => {
//                             let formatedBill = convertBillIntoRequiredFormat(newBill)
//                             // return res.status(200).send({billNumber:newBill.billNumber,amount:newBill.amount,date:newBill.date,discount:newBill.discount,paymentMode:newBill.paymentMode,time:newBill.time,status:newBill.status,_id:newBill._id,products:products})
//                             console.log(formatedBill)

//                             let promises = formatedBill.products.map((prod: any) => {
//                                 return Product.findByIdAndUpdate(prod._id, { inStock: prod.inStock - prod.buyQuantity })
//                             })

//                             Promise.all(promises).then((val) => {
//                                 console.log(val)
//                                 return res.status(200).send(formatedBill)
//                             }).catch((e) => console.log("ErrorWhileupdaation", e))


//                         }).catch((err) => {
//                             return res.status(503).send("Unable to fetch Generated Bill [ bill Number :: " + result.billNumber)
//                         })
//                     }).catch((err) => {
//                         console.log(err)
//                         return res.status(500).send(err)
//                     })
//                 }
//             }).catch((err) => {
//                 return res.status(503).send("Unable to process request")
//             })
//         }
//     }).catch((Err) => {
//         res.status(503).send("Unable to generate Bill Number")
//     })

// })

const generateBill = async (bill: IBill, res: Response) => {
    const session = await conn.startSession();
    let billOperation: { msg: any, status: number } = { msg: "", status: 0 }
    try {
        session.startTransaction()
        await getValueForNextSequence("billNumber", session).then(async (data) => {
            if (data) {
                bill.billNumber = data?.seq
                await new Bill({ ...bill }).save({ session }).then(async (result) => {
                    console.log(result)
                    result.populate("products.product").then((newBill) => {
                        let formatedBill = convertBillIntoRequiredFormat(newBill)
                        let promises = formatedBill.products.map((prod: any) => {
                            if (prod.inStock - prod.buyQuantity < 0)
                                throw `Insufficient Quantity of ${prod.productName}-${prod.productSize}`
                            return Product.findByIdAndUpdate(prod._id, { inStock: prod.inStock - prod.buyQuantity }, { session })
                        })

                        Promise.all(promises).then(async (val) => {
                            // throw "InvalidError"
                            await session.commitTransaction();
                            billOperation = { status: 200, msg: formatedBill }
                            return res.status(200).send(formatedBill)
                        }).catch(async (err) => {
                            console.log("ErrorWhileUpdating inStock")
                            await session.abortTransaction();
                            billOperation = { status: 409, msg: "Error while updating inStock" }
                            return res.status(409).send("Error while updating inStock")
                        })
                    }).catch(async (err) => {
                        // return res.status(503).send("Unable to fetch Generated Bill [ bill Number :: " + result.billNumber)
                        console.log("ErrorWhileFetchingBill", err)
                        await session.abortTransaction();
                        if (err.indexOf("Insufficient") !== -1)
                            return res.status(422).send(err)
                        billOperation = { status: 503, msg: "Unable to fetch Generated Bill [ bill Number :: " + result.billNumber }
                        return res.status(503).send("Unable to fetch Generated Bill [ bill Number :: " + result.billNumber)
                    })
                }).catch(async (err) => {
                    console.log("ErrorWhileSavingBill")
                    await session.abortTransaction();
                    console.log(err)
                    // return res.status(500).send(err)
                    billOperation = { status: 403, msg: err }
                    return res.status(403).send(err)
                })
            }
        }).catch(async (err) => {
            console.log("ErrorWhileGeneratingNewBillNumber")
            await session.abortTransaction();
            billOperation = { status: 503, msg: "Unable to generate Bill Number" }
            return res.status(503).send("Unable to generate Bill Number")
        })
    } catch (err) {
        await session.abortTransaction();
        billOperation = { status: 403, msg: err }
        return res.status(403).send(err)
    }
}

const returnBill = async (bill:IBill,res:Response)=>{
    const session = await conn.startSession();
    try{
        session.startTransaction();
        let promises = bill.products.map((prod: any) => {
            return Product.findByIdAndUpdate(prod._id, { inStock: prod.inStock + prod.buyQuantity }, { session })
        })
        Promise.all(promises).then(async (val) => {
            Bill.findOneAndUpdate({billNumber:bill.billNumber},{status:"Return Accepted"}).then(async(result)=>{
                if(result){
                    await session.commitTransaction();
                    return res.status(200).send(`Return Accepted Successfully [ Bill Number :: ${bill.billNumber} ]`)
                }else{
                    console.log("Bill Not found")
                    await session.abortTransaction();
                    return res.send("Bill Not found").status(404)
                }
            }).catch(async(err)=>{
                console.log("ErrorWhileUpdating Bill Status",err)
                await session.abortTransaction();
                return res.status(409).send("Error while updating Bill status")
            })
        }).catch(async (err) => {
            console.log("ErrorWhileUpdating inStock")
            await session.abortTransaction();
            return res.status(409).send("Error while updating inStock")
        })
    }catch(err){
        await session.abortTransaction(); 
        return res.status(403).send(err)
    }
}

router.patch("/", async (req: Request, res: Response) => {
    const bill:IBill = req.body
    if(bill===undefined)
        return res.send(403).send("Invalid Bill")
    else
        await returnBill(bill,res)
})

router.post("/", async (req: Request, res: Response) => {
    let bill: IBill = req.body
    await generateBill(bill, res)
})


export { router as BillingEndpoints }