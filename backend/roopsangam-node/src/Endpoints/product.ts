import express, { Request, response, Response } from "express";
import bcrypt from "bcrypt";
import { Product } from "../Schemas/product_schema";
import { IProduct } from "../Interfaces/product";

const router = express.Router();

router.get("/", (req: Request, res: Response) => {
    Product.find().then((data) => {
        if (data)
            return res.status(200).send(data)
    }).catch((err) => {
        return res.status(500).send(err)
    })
})

router.get("/:id",(req:Request,res:Response)=>{
    Product.findById(req.params.id).then(result=>{
        if(result)
            return res.status(200).send(result)
        else
        return res.status(404).send("Product Not Found :: "+req.params.id)
    }).catch((err)=>{
        if(err.name==="CastError")
            return res.status(400).send("Invalid Product ID")
        return res.status(500).send(err)
    })
})

router.post("/", (req: Request, res: Response) => {
    const product: IProduct = req.body;
    if (product.productName === undefined || product.productName === "")
        return res.status(400).send("Product Name is Required")
    else if (product.productCategory === undefined || product.productCategory === "")
        return res.status(400).send("Product Category is Required")
    else if (product.productSubCategory === undefined || product.productSubCategory === "")
        return res.status(400).send("Product subCategory is Required")
    else if (product.productPrice === undefined || product.productPrice === null || product.productPrice===0)
        return res.status(400).send("Product Price is Required")
    else if (product.inStock === undefined || product.inStock === null)
        return res.status(400).send("Product in stock is Required")
    else if (product.productSize === undefined || product.productSize === "")
        return res.status(400).send("Product Size is Required")
    else {
        new Product({ ...product }).save().then((product) => {
            return res.status(200).send(product)
        }).catch((err) => {
            if(err.name === "MongoServerError")
                return res.status(409).send(err.message)
            return res.status(500).send("Error => " + err.message)
        })
    }
})

router.delete("/:id", (req: Request, res: Response) => {
    Product.deleteOne({ "_id": req.params.id }).then((result) => {
        if (result.deletedCount !== 0)
            return res.status(200).send("Product deleted successfully...!")
        else
            return res.status(404).send("Product Not Found with :: " + req.params.id)
    }).catch((err) => {
        if (err.name === "CastError")
            return res.status(400).send("Invalid Product ID")
        return res.status(500).send("Error => " + err.message)
    })
})

router.put("/:id", (req: Request, res: Response) => {
    Product.findById(req.params.id).then((result) => {
        if (!result)
            return res.status(400).send("Product Not Found with :: " + req.params.id)
        result.updateOne(req.body).then((result)=>{
            res.status(200).send(result)
        }).catch((err)=>{
            if(err.name === "MongoServerError")
                return res.status(409).send(err.message)
            return res.status(500).send("Error => " + err.message)
        })
    }).catch((err) => {
        console.log(err)
        if (err.name === "CastError")
            return res.status(400).send("Invalid Product ID")
        return res.status(500).send("Error => " + err.message)
    })
})

router.post("/valuesByFieldName",(req:Request,res:Response)=>{
    Product.find().distinct(req.body.fieldName).then(data=>{
        return res.send(data)
    }).catch((err)=>{
        return res.status(500).send(err)
    })
})

router.get("/getByCategory/:category",(req:Request,res:Response)=>{
    Product.find({productCategory:req.params.category}).then((products)=>{
        if(products)
            return res.status(200).send(products)
        else
            return res.status(404).send("No Products Found")
    }).catch((err)=>{
        return res.status(500).send(err.message)
    })
})


export { router as ProductEndpoints }