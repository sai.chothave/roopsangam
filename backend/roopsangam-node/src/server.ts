import express, { Express } from 'express';
import dotenv from 'dotenv';
import { json } from 'body-parser';
import { Endpoints } from "./endpoints"
import cors from "cors";
import { ProductEndpoints } from './Endpoints/product';
import { BillingEndpoints } from './Endpoints/bill';
import { conn } from "./MongoConnection"
import { Server, Socket } from 'socket.io';
import { ClientToServerEvents, InterServerEvents, ServerToClientEvents, SocketData } from './Interfaces/ISocket';
import { registerScannerHandler } from './Socket/Scanner';
import { registerDesktopHandler } from './Socket/Desktop';
import { createServer } from "https";
import { readFileSync } from 'fs';


dotenv.config();
const port = process.env.PORT;
const mongoURI = process.env.MONGO_URL || "";

const app: Express = express();
app.use(json())
app.use(cors())


conn.get("roopsangam")


// mongoose.connect("mongodb+srv://sm4989:Sam_4989@cluster0.wemnv.mongodb.net/roopsangam?retryWrites=true&w=majority", {
//     useNewUrlParser: true,
//     useUnifiedTopology: true,
// } as ConnectOptions, () => {
//     console.log(mongoURI)
//     console.log("Connected to Database")
// })

app.use(Endpoints)
app.use("/product", ProductEndpoints)
app.use("/billing", BillingEndpoints)





const server = app.listen(port, () => {
    console.log(`[Roopsangam - server]: Server is running at https://localhost:${port}`);
});

// const io = new Server<ClientToServerEvents, ServerToClientEvents, InterServerEvents, SocketData>({ cors: { origin: "*" } });

// console.log("file",readFileSync("./SSL/key.pem"))

const httpServer = createServer({
    key:readFileSync(".\/src\/SSL\/key1.pem"),
    cert:readFileSync(".\/src\/SSL\/cert4.pem")
});

const io = new Server<ClientToServerEvents, ServerToClientEvents, InterServerEvents, SocketData>(httpServer, { cors: { origin: "*" }, pingTimeout: 2500, pingInterval: 5000 });

const onJoin = async (data: any, socket: Socket) => {
    if (io.sockets.adapter.rooms.get(data.deviceType)?.size === undefined || io.sockets.adapter.rooms.get(data.deviceType)?.size! < 1) {
        if (data.deviceType === "desktop") {
            await socket.join("desktop")
            socket.data.deviceType = "desktop"
            socket.to("scanner").emit("alert", "success", "System Online")
        } else if (data.deviceType === "scanner" && !socket.rooms.has("scanner")) {
            await socket.join("scanner")
            socket.data.deviceType = "scanner"
            socket.to("desktop").emit("alert", "success", "Scanner Online")
        }
    } else {
        console.log("Already Connected")
        socket.emit("alert", "error", `${data.deviceType} is already connected`)
        socket.disconnect(true);
    }
}


const onConnection = (socket: Socket<ClientToServerEvents, ServerToClientEvents, InterServerEvents, SocketData>) => {
    socket.on("join", (data: any) => { onJoin(data, socket) })

    registerDesktopHandler(io,socket);
    registerScannerHandler(io,socket)

    socket.on("disconnecting",(reason:string)=>{
        if (reason === "ping timeout") {
            socket.disconnect(true)
            socket.to((socket.data.deviceType === "scanner") ? "desktop" : "scanner").emit("alert", "warning", `${socket.data.deviceType} is Disconnected`)
        }
    })

    socket.on("disconnect", (reason: string) => {
        if (reason === "transport close") {
            socket.disconnect(true)
            socket.to((socket.data.deviceType === "scanner") ? "desktop" : "scanner").emit("alert", "error", `${socket.data.deviceType} is Disconnected`)
        }
    })
}

io.on("connection", onConnection)


httpServer.listen(8081).on("error",(error)=>{
    console.log(error)
})


// io.on("connection", (socket) => {
//     socket.on("join", async (data: any) => {
//         if (io.sockets.adapter.rooms.get(data.deviceType)?.size === undefined || io.sockets.adapter.rooms.get(data.deviceType)?.size! < 1) {
//             if (data.deviceType === "desktop") {
//                 await socket.join("desktop")
//                 socket.data.deviceType = "desktop"
//                 socket.to("scanner").emit("alert", "success", "System Online")

//             } else if (data.deviceType === "scanner" && !socket.rooms.has("scanner")) {
//                 await socket.join("scanner")
//                 socket.data.deviceType = "scanner"
//                 socket.to("desktop").emit("alert", "success", "Scanner Online")
//             }
//         } else {
//             console.log("Already Connected")
//             socket.emit("alert", "error", `${data.deviceType} is already connected`)
//             socket.disconnect(true);
//         }
//     });

//     socket.on("isScannerOnline", async () => {
//         if (io.sockets.adapter.rooms.get("scanner")?.size !== undefined || io.sockets.adapter.rooms.get("scanner")?.size === 1) {
//             socket.to("desktop").emit("alert", "success", "Scanner Online")
//         } else {
//             socket.to("desktop").emit("alert", "error", "Scanner Offline")
//         }
//     })

//     socket.on("isDesktopOnline", async () => {
//         if (io.sockets.adapter.rooms.get("desktop")?.size !== undefined || io.sockets.adapter.rooms.get("desktop")?.size === 1) {
//             socket.to("scanner").emit("alert", "success", "System Online")
//         } else {
//             socket.to("scanner").emit("alert", "error", "System Offline")
//         }
//     })

//     socket.on("disconnect", async (reason: any) => {
//         if (reason === "transport close")
//             socket.to("desktop").emit("alert", "error", "Scanner Offline")
//     })

//     socket.on("openBillingWindow", async () => {
//         socket.to("desktop").emit("openBillingWindow")
//     })

//     socket.on("error", (error: Error) => {
//         console.log(error);
//         socket.emit("alert", "error", `${error}`)
//     })

// });
