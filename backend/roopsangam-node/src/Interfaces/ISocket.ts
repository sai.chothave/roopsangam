import mongoose, { Mongoose, VirtualTypeOptions } from "mongoose";

export interface ServerToClientEvents {
    noArg: () => void;
    basicEmit: (a: number, b: string, c: Buffer) => void;
    withAck: (d: string, callback: (e: number) => void) => void;
    alert : (type:"error"|"success"|"info"|"warning",msg:string)=>void;
    openBillingWindow:()=>void;
    billingWindowStatus:(status:boolean)=>void;
    getBillingWindowStatus:()=>void;
    billingWindowClosed:()=>void;
    billingWindowOpened:()=>void;

    addProduct:(productID:mongoose.Types.ObjectId)=>void;
    productAdded:(products:Array<any>)=>void;

    reset:()=>void;
    errorWhileAddingProduct:(err:string)=>void;
    acceptOnlinePayment:(bill:any)=>void;
}

export interface ClientToServerEvents {
    join:(data:any)=>void;
    isScannerOnline:()=>void;
    isDesktopOnline:()=>void;
    openBillingWindow:()=>void;
    getBillingWindowStatus:()=>void;
    billingWindowStatus:(status:boolean)=>void;
    billingWindowClosed:()=>void;
    billingWindowOpened:()=>void;

    addProduct:(productID:mongoose.Types.ObjectId)=>void;
    productAdded:(products:Array<any>)=>void;
    errorWhileAddingProduct:(err:string)=>void;
    acceptOnlinePayment:(bill:any)=>void;
}

export interface InterServerEvents {
    ping: () => void;
}

export interface SocketData {
    name: string;
    age: number;
    deviceType:string;
}