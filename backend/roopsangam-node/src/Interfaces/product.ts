export  interface IProduct {
    productName: string,
    productCategory: string,
    productSubCategory: string,
    productPrice: number,
    inStock: number,
    productCode:string,
    productSize:string
}

export  interface IProducts {
    products: Array<IProduct>,
    filters: Array<string>
}


export interface IPurchasedProducts extends IProduct{
    buyQuantity? : 0 | number,
    product?:any
}

export interface IPurchasedProducts2 extends IProduct {
    buyQuantity? : 0 | number
}


