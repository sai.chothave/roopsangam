import { Server, Socket } from "socket.io"
import { ClientToServerEvents, ServerToClientEvents, SocketData } from "../Interfaces/ISocket"

export const  registerDesktopHandler = (io:Server,socket:Socket<ClientToServerEvents, ServerToClientEvents, SocketData, any>) => {
    socket.on("isDesktopOnline", async () => {
        if (io.sockets.adapter.rooms.get("desktop")?.size !== undefined || io.sockets.adapter.rooms.get("desktop")?.size === 1) {
            socket.to("scanner").emit("alert", "success", "System Online")
        } else {
            socket.to("scanner").emit("alert", "error", "System Offline")
        }
    })

    socket.on("billingWindowClosed",()=>{
        socket.to("scanner").emit("billingWindowClosed")
    })

    socket.on("billingWindowOpened",()=>{
        socket.to("scanner").emit("billingWindowOpened")
    })

    socket.on("productAdded",(products)=>{
        socket.to("scanner").emit("productAdded",products)
    })

    socket.on("errorWhileAddingProduct",(err)=>{
        socket.to("scanner").emit("errorWhileAddingProduct",err)
    })

    socket.on("acceptOnlinePayment",(bill)=>{
        socket.to("scanner").emit("acceptOnlinePayment",bill)
    })
}   