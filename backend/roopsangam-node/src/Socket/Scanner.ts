import { Server, Socket } from "socket.io"
import { ClientToServerEvents, ServerToClientEvents, SocketData } from "../Interfaces/ISocket"

export const registerScannerHandler = (io: Server, socket: Socket<ClientToServerEvents, ServerToClientEvents, SocketData, any>) => {

    socket.on("isScannerOnline", async () => {
        if (io.sockets.adapter.rooms.get("scanner")?.size !== undefined || io.sockets.adapter.rooms.get("scanner")?.size === 1) {
            socket.to("desktop").emit("alert", "success", "Scanner Online")
        } else {
            socket.to("desktop").emit("alert", "error", "Scanner Offline")
        }
    })

    socket.on("openBillingWindow", async () => {
        socket.to("desktop").emit("openBillingWindow")
    })

    socket.on("getBillingWindowStatus", () => {
        console.log("gettingSTS")
        socket.to("desktop").emit("getBillingWindowStatus")
    })

    socket.on("billingWindowStatus",(status)=>{
        socket.to("scanner").emit("billingWindowStatus",status)
    })

    socket.on("addProduct",(productId)=>{
        socket.to("desktop").emit("addProduct",productId)
    })

}   