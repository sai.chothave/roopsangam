import mongoose from "mongoose";

const productSchema = new mongoose.Schema({
    productName: {
        require: true,
        type: String,
    },
    productCategory: {
        require: true,
        type: String,
    },
    productSubCategory: {
        require: true,
        type: String,
    },
    productPrice: {
        require: true,
        type: Number,
    },
    inStock: {
        require: true,
        type: Number,
        min:0
    },
    productCode: {
        type: String,
    },
    productSize: {
        require: true,
        type: String,
    },
})

productSchema.index({"productName":1,"productCategory":1,"productSize":1,"productSubCategory":1},{unique:true})

const Product = mongoose.model("Product", productSchema);

export { Product }