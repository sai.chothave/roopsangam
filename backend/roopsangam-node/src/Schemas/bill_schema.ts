import mongoose from "mongoose";
import { IPurchasedProducts } from "./../Interfaces/product"
import { Product } from "./product_schema";

// const purchedProducts = new Product()
// purchedProducts.add({
//     buyQuantity:{type:Number}
// })

const billSchema = new mongoose.Schema({
    billNumber: { type: Number, require: true, unique: true },
    products: [{
        product: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Product'
        },
        buyQuantity: Number
    }],
    paymentMode: { type: String, require: true, default: "Cash" },
    date: { type: Date, require: true, default: Date.now },
    time: { type: String, require: true },
    amount: { type: Number, require: true },
    discount: { type: Number, require: true },
    status: { type: String, require: true, default: "Paid" }
})

// billSchema.index({"productName":1,"productCategory":1,"productSize":1},{unique:true})

const Bill = mongoose.model("Bill", billSchema,"bills");

export { Bill }